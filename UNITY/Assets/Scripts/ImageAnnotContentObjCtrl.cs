/**
 * @file ImageAnnotContentObjCtrl.cs 
 * @date 2018/07/23 
 * @author KIMJISOO(kind.jamie@gmail.com)
 * @brief 이미지 어노테이션 컨텐츠 오브젝트 컨트롤 스크립트
 */
using UnityEngine;
using UnityEngine.UI;

/**
 * @class ImageAnnotContentObjCtrl
 * @date 2018/07/23
 * @brief 이미지 어노테이션 컨텐츠 오브젝트 컨트롤
 * ImageAnnotContentObj의 컨트롤
 */
public class ImageAnnotContentObjCtrl : MonoBehaviour
{
    public Text txtName;        /** 어노테이션 이름 표시 필드 */
    public RawImage imgImage;   /** 어노테이션 이미지 표시 필드 */

    /**
     * @brief 어노테이션 내용 표시
     * @param _name 어노테이션 이름
     * @param _texture 이미지 필드의 텍스쳐
     */
    public void Set(string _name, Texture2D _texture)
    {
        this.txtName.text = _name;

        Texture texture = null;
        if (this.imgImage.texture != null)
        {
            texture = this.imgImage.texture;
        }
        this.imgImage.texture = _texture;
        if (texture != null && !texture.Equals(_texture)) {
            Destroy(texture);   
        }

        if (this.imgImage.texture != null)
        {
            this.imgImage.color = Color.white;    
        }
        else
        {
            this.imgImage.color = Color.clear;
        }
    }
}
