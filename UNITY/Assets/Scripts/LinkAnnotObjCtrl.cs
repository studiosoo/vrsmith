﻿/**
 * @file LinkAnnotObjCtrl.cs 
 * @date 2018/08/10 
 * @author KIMJISOO(kind.jamie@gmail.com)
 * @brief 링크 어노테이션 오브젝트 컨트롤 스크립트
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * @class LinkAnnotObjCtrl
 * @date 2018/08/10
 * @brief 링크 어노테이션 오브젝트 컨트롤
 * LinkAnnotObj의 컨트롤
 */
public class LinkAnnotObjCtrl : MonoBehaviour {
    
    //STATIC
    static bool OnMoveLock;             /** 링크 이동 금지 플래그 */

    //CONTENT
    public delegate void _MoveToScene(string _scene);
    public _MoveToScene MoveToScene;    /** 링크 이동하기 */
    bool Collided = false;              /** AnnotAimView와 충돌 여부 */     

    //INFO
    public int Idx;                     /** 인텍스, 어노테이션 종류별로 유일함, 0~ */
    public string Name;                 /** 이름 */
    public Vector3 Angle;               /** 파노라믹 뷰 위의 위치, 0 <= x < 360 -90 <= y <= 90 */
    public string CreatedTime;          /** 생성 시간 */
    public string ModifiedTime;         /** 수정된 시간 */
    public string SceneName;            /** Scene 이름, 어노테이션 이름과 동일 */

    //PUBLIC
    /**
     * @brief Content 표시 설정
     * @param _collided Annot Aim 충돌 여부, 조준됨
     */
    public void OnCollided(bool _collided)
    {
        if (_collided && !this.Collided)
        {
            if (!LinkAnnotObjCtrl.OnMoveLock) MoveToScene(this.SceneName);
        }
        this.Collided = _collided;
    }

    /**
     * @brief 어노테이션 정보 설정
     * @param _idx 인텍스, 어노테이션 종류별로 유일함, 0~
     * @param _name 이름
     * @param _angle 파노라믹 뷰 위의 위치
     * @param _ctime 생성 시간
     * @param _mtime 수정된 시간
     * @param _scene
     */
    public void Set(int _idx, string _name, Vector3 _angle, string _ctime, string _mtime, string _scene)
    {
        this.Idx = _idx;
        this.Name = _name;
        this.Angle = _angle;
        this.CreatedTime = _ctime;
        this.ModifiedTime = _mtime;
        this.SceneName = _scene;
    }

    //STATIC
    /**
     * @brief 링크 이동 플래그 설정
     * @param _lock 설정 여부
     */
    static public void SetLock(bool _lock)
    {
        LinkAnnotObjCtrl.OnMoveLock = _lock;
    }
}
