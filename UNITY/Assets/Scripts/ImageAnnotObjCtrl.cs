/**
 * @file ImageAnnotObjCtrl.cs 
 * @date 2018/07/23 
 * @author KIMJISOO(kind.jamie@gmail.com)
 * @brief 이미지 어노테이션 오브젝트 컨트롤 스크립트
 */
using System.IO;
using System.Collections;
using UnityEngine;

/**
 * @class ImageAnnotObjCtrl
 * @date 2018/07/23
 * @brief 이미지 어노테이션 오브젝트 컨트롤
 * ImageAnnotObj의 컨트롤
 */
public class ImageAnnotObjCtrl : MonoBehaviour
{
    //CONTENT
    public delegate void _HideAnnotContent(GameObject _obj);
    public _HideAnnotContent HideAnnotContent;  /** 어노테이션 내용 가리기 */
    public delegate void _ShowAnnotContent(GameObject _obj, string _name, Texture2D _texture);
    public _ShowAnnotContent ShowAnnotContent;  /** 어노테이션 내용 보이기 */
    bool Collided = false;              /** AnnotAimView와 충돌 여부 */
    public bool OnEdit = false;

    //INFO
    public int Idx;                     /** 인텍스, 어노테이션 종류별로 유일함, 0~ */
    public string Name;                 /** 이름 */
    public Vector3 Angle;               /** 파노라믹 뷰 위의 위치, 0 <= x < 360 -90 <= y <= 90 */
    public string CreatedTime;          /** 생성 시간 */
    public string ModifiedTime;         /** 수정된 시간 */
    public string ImagePath;            /** 이미지 이름, 확장자 포함 */
    public int ImageRotation;           /** 이미지 회전 값, {0, 90, 180, 270} */
    public Texture2D Image = null;      /** 이미지 */

    //PUBLIC
    /**
     * @brief Content 표시 설정
     * @param _collided Annot Aim 충돌 여부, 조준됨
     */
    public void OnCollided(bool _collided)
    {
        if (this.OnEdit) _collided = true;
        if (_collided && !this.Collided)
        {
            ShowAnnotContent(this.gameObject, this.Name, this.Image);
        }
        if (!_collided && this.Collided)
        {
            HideAnnotContent(this.gameObject);
        }
        this.Collided = _collided;
    }

    /**
     * @brief 어노테이션 정보 설정
     * @param _idx 인텍스, 어노테이션 종류별로 유일함, 0~
     * @param _name 이름
     * @param _angle 파노라믹 뷰 위의 위치
     * @param _ctime 생성 시간
     * @param _mtime 수정된 시간
     * @param _imgPath 이미지 이름, 확장자 포함
     * @param _imgRot 이미지 회전 값, {0, 90, 180, 270}
     */
    public void Set(int _idx, string _name, Vector3 _angle, string _ctime, string _mtime, string _imgPath, int _imgRot)
    {
        this.Collided = false;

        this.Idx = _idx;
        this.Name = _name;
        this.Angle = _angle;
        this.CreatedTime = _ctime;
        this.ModifiedTime = _mtime;
        this.ImagePath = _imgPath;
        this.ImageRotation = _imgRot;
        this.SetImage(ImageAnnotObjCtrl.LoadTexture(this.ImagePath, this.ImageRotation));
    }

    /**
    * @brief 이미지 어노테이션의 이미지에 텍스쳐 설정
    * @param _texture 새 텍스쳐
    * 현재 설정된 텍스쳐를 삭제하고 설정함
    */
    public void SetImage(Texture2D _texture)
    {
        Texture texture = null;
        if (this.Image != null)
        {
            texture = this.Image;
        }
        this.Image = _texture;
        if (texture != null && !texture.Equals(_texture))
        {
            Destroy(texture);
        }
    }

    //STATIC
    /**
     * @brief 텍스쳐 로드
     * @param _path 이미지 파일을 경로
     * @param _rotate 이미지 회전 값, {0, 90, 180, 270}
     * @param _scale
     * @returns 원본 이미지의 1/10 크기의 회전된 이미지 
     */
    static public Texture2D LoadTexture(string _path, int _rotate = 0, float _scale = 1.0f)
    {           
        Texture2D texture = null;

        string path = Path.Combine(MainCtrl.TEMP_DIR, _path);
        if (System.IO.File.Exists(path))
        {
            texture = new Texture2D(2, 2);
            byte[] fileData = System.IO.File.ReadAllBytes(path);
            if (texture.LoadImage(fileData))
            {
                texture = ImageAnnotObjCtrl.ScaleTexture(texture,
                                                         (int)((float)texture.width * _scale),
                                                         (int)((float)texture.height * _scale));
                for (int r = 0; r < _rotate; r += 90)
                {
                    texture = ImageAnnotObjCtrl.RotateTexture(texture, true);
                }
                Debug.Log("SUCCESS LoadTexture " + _path + " " + _rotate.ToString());
            }
        }

        return texture;
    }

    /**
     * @brief 이미지의 GPS 정보를 리턴
     * @param _path 이미지 파일 이름
     * @returns 위도, 경도 정보가 설정된 Vector2
     */
    static public Vector2 GPSLocation(string _path)
    {
        string path = Path.Combine(MainCtrl.TEMP_DIR, _path);
        ExifLib.JpegInfo jpi = ExifLib.ExifReader.ReadJpeg(System.IO.File.ReadAllBytes(path), path);

        double latitude = 0.0;
        if (jpi.GpsLatitude.Length == 3) {
            latitude = jpi.GpsLatitude[0] + jpi.GpsLatitude[1] / 60 + jpi.GpsLatitude[2] / 3600;
        }
        double longitude = 0.0;
        if (jpi.GpsLongitude.Length == 3)
        {
            longitude = jpi.GpsLongitude[0] + jpi.GpsLongitude[1] / 60 + jpi.GpsLongitude[2] / 3600;
        }

        return new Vector2((float)latitude, (float)longitude);
    }

    /**
     * @brief 텍스쳐 크기 변경 
     * @param _source 원본 이미지
     * @param _width 결과 이미지의 가로 크기
     * @param _height 결과 이미지의 세로 크기
     * @returns 크기가 변환된 이미지
     */
    static public Texture2D ScaleTexture(Texture2D _source, int _width, int _height)
    {
        Texture2D result = new Texture2D(_width, _height, _source.format, true);
        Color[] rpixels = result.GetPixels(0);
        float incX = (1.0f / (float)_width);
        float incY = (1.0f / (float)_height);
        for (int px = 0; px < rpixels.Length; px++)
        {
            rpixels[px] = _source.GetPixelBilinear(incX * ((float)px % _width), incY * ((float)Mathf.Floor(px / _width)));
        }
        result.SetPixels(rpixels, 0);
        result.Apply();
        Destroy(_source);
        return result;
    }

    /**
     * @brief 텍스쳐 회전 
     * @param _source 원본 이미지
     * @param _angle 회전 값
     * @returns 회전된 이미지
     */
    static public Texture2D RotateTexture(Texture2D _source, int _angle)
    {
        Texture2D result;
        result = new Texture2D(_source.width, _source.height);
        Color32[] pix1 = result.GetPixels32();
        Color32[] pix2 = _source.GetPixels32();
        int W = _source.width;
        int H = _source.height;
        int x = 0;
        int y = 0;
        Color32[] pix3 = RotateSquare(_source, pix2, (System.Math.PI / 180 * (double)_angle));
        for (int j = 0; j < H; j++)
        {
            for (var i = 0; i < W; i++)
            {
                //pix1[result.width/2 - _source.width/2 + x + i + result.width*(result.height/2-_source.height/2+j+y)] = pix2[i + j*_source.width];
                pix1[result.width / 2 - W / 2 + x + i + result.width * (result.height / 2 - H / 2 + j + y)] = pix3[i + j * W];
            }
        }
        result.SetPixels32(pix1);
        result.Apply();
        Destroy(_source);
        return result;
    }

    /**
     * @brief 텍스쳐 회전 
     * @param _source 원본 이미지
     * @param _arr 원본 컬러 리스트
     * @param _phi 회전 값
     * @returns 컬러 리스트
     */
    static public Color32[] RotateSquare(Texture2D _source, Color32[] _arr, double _phi)
    {
        int x;
        int y;
        int i;
        int j;
        double sn = System.Math.Sin(_phi);
        double cs = System.Math.Cos(_phi);
        Color32[] arr2 = _source.GetPixels32();
        int W = _source.width;
        int H = _source.height;
        int xc = W / 2;
        int yc = H / 2;
        for (j = 0; j < H; j++)
        {
            for (i = 0; i < W; i++)
            {
                arr2[j * W + i] = new Color32(0, 0, 0, 0);
                x = (int)(cs * (i - xc) + sn * (j - yc) + xc);
                y = (int)(-sn * (i - xc) + cs * (j - yc) + yc);
                if ((x > -1) && (x < W) && (y > -1) && (y < H))
                {
                    arr2[j * W + i] = _arr[y * W + x];
                }
            }
        }
        return arr2;
    }

    /**
     * @brief 텍스쳐 회전 
     * @param _source 원본 이미지
     * @param _clockwise 시계 방향
     * @returns 회전된 이미지
     */
    static public Texture2D RotateTexture(Texture2D _source, bool _clockwise)
    {
        Color32[] original = _source.GetPixels32();
        Color32[] rotated = new Color32[original.Length];
        int w = _source.width;
        int h = _source.height;

        int iRotated, iOriginal;

        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                iRotated = (i + 1) * h - j - 1;
                iOriginal = _clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[iRotated] = original[iOriginal];
            }
        }

        Texture2D rotatedTexture = new Texture2D(h, w);
        rotatedTexture.SetPixels32(rotated);
        rotatedTexture.Apply();
        Destroy(_source);
        return rotatedTexture;
    }
}
