var searchData=
[
  ['newcontentxml',['NewContentXML',['../class_proj_ctrl.html#ad2b343e6aa7f1853fa368953ee499698',1,'ProjCtrl']]],
  ['newidx',['NewIdx',['../class_image_annot_view_ctrl.html#ae8bf6a98fd6beba991aab8a18bd58883',1,'ImageAnnotViewCtrl.NewIdx()'],['../class_link_annot_view_ctrl.html#ac951905322e5a37111c548c8f65a89d1',1,'LinkAnnotViewCtrl.NewIdx()'],['../class_text_annot_view_ctrl.html#a1ac6e9a03f4c6922a1b6f75833ac754c',1,'TextAnnotViewCtrl.NewIdx()']]],
  ['newimageannot',['NewImageAnnot',['../class_annot_edit_view_ctrl.html#a6034bde0e05cf3bf771cd310b9d6bf5f',1,'AnnotEditViewCtrl.NewImageAnnot()'],['../class_proj_ctrl.html#a97610775f66578df59539407a036cde7',1,'ProjCtrl.NewImageAnnot()']]],
  ['newlinkannot',['NewLinkAnnot',['../class_proj_ctrl.html#a7adb807e7337a31a3711874b6c57817b',1,'ProjCtrl']]],
  ['newscenenode',['NewSceneNode',['../class_proj_ctrl.html#a85b7fc42966a28f1002b62ead292c30b',1,'ProjCtrl']]],
  ['newtextannot',['NewTextAnnot',['../class_annot_edit_view_ctrl.html#ae61a4a8a2723f817f70c9c4f0e470d2a',1,'AnnotEditViewCtrl.NewTextAnnot()'],['../class_proj_ctrl.html#abd4cb49dfac01d09b86b0f1b7dabe374',1,'ProjCtrl.NewTextAnnot()']]],
  ['now',['Now',['../class_proj_ctrl.html#a5a9419c1dd17d04382c4f1d92492ca85',1,'ProjCtrl']]]
];
