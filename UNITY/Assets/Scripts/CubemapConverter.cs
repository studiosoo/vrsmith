﻿/**
 * @file CubemapConverter.cs 
 * @date 2017/08/07 
 * @author Max Play
 * @brief Assign the generated texture to the cubemap
 * https://stackoverflow.com/questions/45032579/editing-a-cubemap-skybox-from-remote-image
 */
using UnityEngine;

/**
 * @class CubemapConverter
 */
public class CubemapConverter : MonoBehaviour
{
    /**
     * These are the faces of a cube
     */
    static private Vector3[][] faces =
    {
        new Vector3[] {
            new Vector3(1.0f, 1.0f, -1.0f),
            new Vector3(1.0f, 1.0f, 1.0f),
            new Vector3(1.0f, -1.0f, -1.0f),
            new Vector3(1.0f, -1.0f, 1.0f)
        },
        new Vector3[] {
            new Vector3(-1.0f, 1.0f, 1.0f),
            new Vector3(-1.0f, 1.0f, -1.0f),
            new Vector3(-1.0f, -1.0f, 1.0f),
            new Vector3(-1.0f, -1.0f, -1.0f)
        },
        new Vector3[] {
            new Vector3(-1.0f, 1.0f, 1.0f),
            new Vector3(1.0f, 1.0f, 1.0f),
            new Vector3(-1.0f, 1.0f, -1.0f),
            new Vector3(1.0f, 1.0f, -1.0f)
        },
        new Vector3[] {
            new Vector3(-1.0f, -1.0f, -1.0f),
            new Vector3(1.0f, -1.0f, -1.0f),
            new Vector3(-1.0f, -1.0f, 1.0f),
            new Vector3(1.0f, -1.0f, 1.0f)
        },
        new Vector3[] {
            new Vector3(-1.0f, 1.0f, -1.0f),
            new Vector3(1.0f, 1.0f, -1.0f),
            new Vector3(-1.0f, -1.0f, -1.0f),
            new Vector3(1.0f, -1.0f, -1.0f)
        },
        new Vector3[] {
            new Vector3(1.0f, 1.0f, 1.0f),
            new Vector3(-1.0f, 1.0f, 1.0f),
            new Vector3(1.0f, -1.0f, 1.0f),
            new Vector3(-1.0f, -1.0f, 1.0f)
        }
    };

    /**
    * @brief Convert 2D Texture to Cubemap
    * @param _source Texture2D
    * @returns Cubemap
    */
    static public Cubemap Convert(Texture2D _source)
    {      
        // new cubemap 
        Cubemap c = new Cubemap(_source.height, TextureFormat.RGBA32, false);

        Color[] CubeMapColors;

        for (int i = 0; i < 6; i++)
        {
            CubeMapColors = CreateCubemapTexture(_source, _source.height, (CubemapFace)i);
            c.SetPixels(CubeMapColors, (CubemapFace)i);
        }
        // we set the cubemap from the texture pixel by pixel
        c.Apply();

        //Destroy all unused textures
        Destroy(_source);
        Texture2D[] texs = FindObjectsOfType<Texture2D>();
        for (int i = 0; i < texs.Length; i++)
        {
            Destroy(texs[i]);
        }

        return c;
    }

    /**
     * @brief Generates a Texture that represents the given face for the cubemap.
     * @param _source Texture2D
     * @param _resolution The targetresolution in pixels
     * @param _face The target face
     * @returns Color[]
     */
    static private Color[] CreateCubemapTexture(Texture2D _source, int _resolution, CubemapFace _face)
    {
        Texture2D texture = new Texture2D(_resolution, _resolution, TextureFormat.RGB24, false);

        Vector3 texelX_Step = (faces[(int)_face][1] - faces[(int)_face][0]) / _resolution;
        Vector3 texelY_Step = (faces[(int)_face][3] - faces[(int)_face][2]) / _resolution;

        float texelSize = 1.0f / _resolution;
        float texelIndex = 0.0f;

        //Create textured face
        Color[] cols = new Color[_resolution];
        for (int y = 0; y < _resolution; y++)
        {
            Vector3 texelX = faces[(int)_face][0];
            Vector3 texelY = faces[(int)_face][2];
            for (int x = 0; x < _resolution; x++)
            {
                cols[x] = Project(_source, Vector3.Lerp(texelX, texelY, texelIndex).normalized);
                texelX += texelX_Step;
                texelY += texelY_Step;
            }
            texture.SetPixels(0, y, _resolution, 1, cols);
            texelIndex += texelSize;
        }
        texture.wrapMode = TextureWrapMode.Clamp;
        texture.Apply();

        Color[] colors = texture.GetPixels();
        Destroy(texture);

        return colors;
    }

    /**
     * @brief Projects a directional vector to the texture using spherical mapping
     * @param _source Texture2D
     * @param _direction The direction in which you view
     * @returns Color
     */
    static private Color Project(Texture2D source, Vector3 direction)
    {
        float theta = Mathf.Atan2(direction.z, direction.x) + Mathf.PI / 180.0f;
        float phi = Mathf.Acos(direction.y);

        int texelX = (int)(((theta / Mathf.PI) * 0.5f + 0.5f) * source.width);
        if (texelX < 0) texelX = 0;
        if (texelX >= source.width) texelX = source.width - 1;
        int texelY = (int)((phi / Mathf.PI) * source.height);
        if (texelY < 0) texelY = 0;
        if (texelY >= source.height) texelY = source.height - 1;

        return source.GetPixel(texelX, source.height - texelY - 1);
    }
}