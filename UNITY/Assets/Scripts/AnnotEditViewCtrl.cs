﻿/**
 * @file AnnotEditViewCtrl.cs 
 * @date 2018/07/23 
 * @author KIMJISOO(kind.jamie@gmail.com)
 * @brief 어노테이션 편집 뷰 컨트롤 스크립트
 */
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using ImageAndVideoPicker;

/**
 * @class AnnotEditViewCtrl
 * @date 2018/07/23
 * @brief 어노테이션 편집 뷰 컨트롤
 * AnnotEditView의 컨트롤
 */
public class AnnotEditViewCtrl : MonoBehaviour
{
    //EXTERNAL VIEW
    public GameObject AnnotListView;            /** 어노테이션 리스트 뷰 */
    public GameObject TextAnnotView;            /** 텍스트 어노테이션 뷰, 텍스트 어노테이션 오브젝트 리스트 참조 */
    public GameObject ImageAnnotView;           /** 이미지 어노테이션 뷰, 이미지 어노테이션 오브젝트 리스트 참조 */
    public GameObject AnnotView;                /** 이미지 어노테이션 뷰, 이미지 어노테이션 오브젝트 리스트 참조 */
    //EXTERNAL VIEW

    //MENU
    public Button btnBack;                      /** 닫기 버튼 */
    public Button btnTextAnnot;                 /** 텍스트 어노테이션 선택 버튼 */
    public Button btnImageAnnot;                /** 이미지 어노테이션 선택 버튼 */
    public Button btnLinkAnnot;                 /** 이미지 어노테이션 선택 버튼 */
    public Button btnSave;                      /** 저장 버튼 */
    public GameObject EditMenuView;             /** 편집 메뉴 버튼 */
    public Button btnDel;                       /** 삭제 버튼 */

    //Common
    public InputField inpName;                  /** 어노테이션 네임 입력 필드 */
    public Text txtAngle;                       /** 어노테이션 위치 표시 필드 */
    //TextAnnot
    public GameObject TextAnnotContentEditView; /** 텍스트 어노테이션 내용 편집 뷰 */
    public InputField inpDesc;                  /** 텍스트 어노테이션 설명 입력 필드 */
    public Button btnPos;                       /** 텍스트 어노테이션 내용 표시 위치 선택 버튼 */
    public Sprite LTPOS;                        /** Left-Top 위치 선택 버튼 이미지 */
    public Sprite RTPOS;                        /** Right-Top 위치 선택 버튼 이미지 */
    public Sprite LBPOS;                        /** Left-Bottom 위치 선택 버튼 이미지 */
    public Sprite RBPOS;                        /** Right-Bottom 위치 선택 버튼 이미지 */
    public Sprite CLPOS;                        /** Cloud 위치 선택 버튼 이미지 */
    private int Pos;                            /** 텍스트 어노테이션 내용 표시 위치 선택 값, 0 ~ 4 */
    //ImageAnnot
    public GameObject ImageAnnotContentEditView;/** 이미지 어노테이션 내용 편집 뷰 */
    public RawImage imgImage;                   /** 이미지 표시 필드 */
    public Button btnImage;                     /** 이미지 설정 버튼 */
    public Button btnLRotate;                   /** 반 시계방향 회전 버튼 */ 
    public Button btnRRotate;                   /** 시계방향 회전 버튼 */
    private string ImagePath;                   /** 이미지 이름, 확장자 포함 */
    private int ImageRotation;                  /** 이미지 회전 값, {0, 90, 180, 270} */
    //LinkAnnot
    public GameObject LinkAnnotContentEditView; /** 이미지 어노테이션 내용 편집 뷰 */
    public RawImage imgLink;                    /** 이미지 표시 필드 */
    public Button btnLink;                      /** 이미지 설정 버튼 */
    public Button btnLScene;                    /** 반 시계방향 회전 버튼 */
    public Button btnRScene;                    /** 시계방향 회전 버튼 */
    private int SceneIdx;                       /** 씬 인덱스, 0~ */
    private string SceneName;                   /** Scene 이름 */
    private string SceneImagePath;              /** 씬 이미지 경로 */
    //Target
    private GameObject TargetObj;               /** 추가/편집의 대상이 되는 오브젝트 */
    private Vector3 OriAngle = Vector3.negativeInfinity;    /** TargetObj의 편집 전 원래 위치, 추가인 경우 Vector3.negativeInfinity */
    private string OriName;                     /** 편집전 어노테이션 이름 */
    private string OriDesc;                     /** 편집전 텍스트 어노테이션 설명 */
    private string OriPos;                      /** 편집전 텍스트 어노테이션 표시 위치 */
    private string OriImagePath;                /** 편집전 이미지 어노테이션 이미지 경로 */
    private int OriImageRotation;               /** 편집전 이미지 어노테이션 이미지 회전 */

    /**
     * @brief 뷰와 필드 초기화, 리스너 등록
     */
    void Start()
    {
        this.btnBack.onClick.AddListener(OnBackButtonClicked);
        this.btnTextAnnot.onClick.AddListener(OnTextAnnotButtonClicked);
        this.btnImageAnnot.onClick.AddListener(OnImageAnnotButtonClicked);
        this.btnLinkAnnot.onClick.AddListener(OnLinkAnnotButtonClicked);
        this.btnSave.onClick.AddListener(OnSaveButtonClicked);
        this.btnDel.onClick.AddListener(OnDelButtonClicked);
            
        //Common
        this.inpName.onEndEdit.AddListener(delegate { OnNameInputEndEdit(); });
        //TextAnnot
        this.TextAnnotContentEditView.SetActive(false);
        this.inpDesc.onEndEdit.AddListener(delegate { OnDescInputEndEdit(); });
        this.btnPos.GetComponent<Image>().sprite = PosToSprite(StringToPos(TextAnnotContentObjCtrl.LTPOS));
        this.btnPos.onClick.AddListener(OnPosButtonClicked);
        //ImageAnnot
        this.ImageAnnotContentEditView.SetActive(false);
        this.btnImage.onClick.AddListener(OnImageButtonClicked);
        this.btnLRotate.onClick.AddListener(OnLRotateButtonClicked);
        this.btnRRotate.onClick.AddListener(OnRRotateButtonClicked);
        //LinkAnnot
        this.LinkAnnotContentEditView.SetActive(false);
        this.btnLink.onClick.AddListener(OnLinkButtonClicked);
        this.btnLScene.onClick.AddListener(OnLSceneButtonClicked);
        this.btnRScene.onClick.AddListener(OnRSceneButtonClicked);

        this.gameObject.SetActive(false);
    }

    /**
     * @brief 추가/편집 중 위치 이동
     */
    void Update()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved)
            {
                this.OnMoved();
            }
        }
    }

    /**
     * @brief 추가/편집 중 대상 오브젝트의 위치 설정
     */
    void OnMoved()
    {
        Vector3 angle = CurAngle();
        this.txtAngle.text = AnnotListCtrl.AngleToString(angle);
        if (this.TargetObj != null)
        {
            if (this.TextAnnotContentEditView.activeSelf)
            {
                this.TargetObj.GetComponent<TextAnnotObjCtrl>().Angle = angle;
            }
            else if (this.ImageAnnotContentEditView.activeSelf)
            {
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().Angle = angle;
            }
            else if (this.LinkAnnotContentEditView.activeSelf)
            {
                this.TargetObj.GetComponent<LinkAnnotObjCtrl>().Angle = angle;
            }
            PanoramicViewCtrl.Set(this.TargetObj, angle);
        }
    }

    /**
     * @brief 비활성화시, 이미지 피커 리스너 삭제
     */
    void OnDisable()
    {
        if (this.TargetObj != null)
        {
            this.TargetObj.transform.parent = null;
            this.TargetObj.SetActive(false);
            DestroyImmediate(this.TargetObj);
            this.TargetObj = null;
        }
    }

    //DELEGATE
    /**
     * @brief 닫기 버튼 클릭 
     */
    void OnBackButtonClicked()
    {
        if (this.OriAngle.Equals(Vector3.negativeInfinity))
        {
            this.ImageAnnotView.GetComponent<ImageAnnotViewCtrl>().HideAnnotContent();
            this.TextAnnotView.GetComponent<TextAnnotViewCtrl>().HideAnnotContent();
            this.TargetObj.transform.parent = null;
            this.TargetObj.SetActive(false);
            DestroyImmediate(this.TargetObj);
        }
        else
        {
            if (this.TextAnnotContentEditView.activeSelf)
            {
                this.TargetObj.GetComponent<TextAnnotObjCtrl>().OnEdit = false;
                this.TargetObj.GetComponent<TextAnnotObjCtrl>().Name = OriName;
                this.TargetObj.GetComponent<TextAnnotObjCtrl>().Angle = OriAngle;
                this.TargetObj.GetComponent<TextAnnotObjCtrl>().Desc = OriDesc;
                this.TargetObj.GetComponent<TextAnnotObjCtrl>().Pos = OriPos;
                this.TargetObj.GetComponent<TextAnnotObjCtrl>().OnCollided(false);
            }
            else if (this.ImageAnnotContentEditView.activeSelf)
            {
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnEdit = false;
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().Name = OriName;
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().Angle = OriAngle;
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().ImagePath = OriImagePath;
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().ImageRotation = OriImageRotation;
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().SetImage(
                    ImageAnnotObjCtrl.LoadTexture(OriImagePath, OriImageRotation));
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnCollided(false);
            }
            else if (this.LinkAnnotContentEditView.activeSelf)
            {
                this.TargetObj.GetComponent<LinkAnnotObjCtrl>().Angle = OriAngle;
            }
            PanoramicViewCtrl.Set(this.TargetObj, this.OriAngle);
            PanoramicViewCtrl.Raycast();
        }
        this.inpName.readOnly = false;
        LinkAnnotObjCtrl.SetLock(false);

        this.TargetObj = null;
        this.gameObject.SetActive(false);
    }

    /**
     * @brief 텍스트 어노테이션 추가 버큰 클릭
     */
    void OnTextAnnotButtonClicked()
    {
        NewAnnot(AnnotListCtrl.TEXT_KEY);
    }

    /**
     * @brief 이미지 어노테이션 추가 버큰 클릭
     */
    void OnImageAnnotButtonClicked()
    {
        NewAnnot(AnnotListCtrl.IMAGE_KEY);
    }

    /**
     * @brief 링크 어노테이션 추가 버큰 클릭
     */
    void OnLinkAnnotButtonClicked()
    {
        NewAnnot(AnnotListCtrl.LINK_KEY);
    }

    /**
     * @brief 어노테이션 저장 버튼 클릭
     * 추가/수정 후 어노테이션 리스트 갱신, 현재 뷰 닫기
     */
    void OnSaveButtonClicked()
    {
        if (this.inpName.text == "") return;

        if (this.TextAnnotContentEditView.activeSelf)
        {
            TextAnnotObjCtrl annotObject = this.TargetObj.GetComponent<TextAnnotObjCtrl>();
            annotObject.OnEdit = false;
            annotObject.OnCollided(false);
            annotObject.Set(annotObject.Idx,
                            CurAngle(),
                            this.PosToString(this.Pos),
                            this.inpName.text,
                            annotObject.CreatedTime,
                            ProjCtrl.Now(),
                            this.inpDesc.text);
            ProjCtrl.TextAnnot(MainCtrl.CurScene, this.TargetObj);

            this.TextAnnotView.GetComponent<TextAnnotViewCtrl>().BuildList();
        }
        else if (this.ImageAnnotContentEditView.activeSelf)
        {
            ImageAnnotObjCtrl annotObject = this.TargetObj.GetComponent<ImageAnnotObjCtrl>();
            annotObject.OnEdit = false;
            annotObject.OnCollided(false);
            annotObject.Set(annotObject.Idx,
                            this.inpName.text,
                            CurAngle(),
                            annotObject.CreatedTime,
                            ProjCtrl.Now(),
                            this.ImagePath,
                            this.ImageRotation);
            ProjCtrl.ImageAnnot(MainCtrl.CurScene, this.TargetObj);

            this.ImageAnnotView.GetComponent<ImageAnnotViewCtrl>().BuildList();
        }
        else if (this.LinkAnnotContentEditView.activeSelf)
        {
            LinkAnnotObjCtrl annotObject = this.TargetObj.GetComponent<LinkAnnotObjCtrl>();
            if (this.SceneIdx == ProjCtrl.SceneCount())
            {
                if (this.SceneName != "" && this.SceneImagePath != "")
                {
                    ProjCtrl.AppendScene();
                    ProjCtrl.SceneName(this.SceneIdx, this.SceneName);
                    ProjCtrl.SceneImage(this.SceneIdx, this.SceneImagePath);
                    ProjCtrl.SceneLocation(this.SceneIdx, ImageAnnotObjCtrl.GPSLocation(this.SceneImagePath));
                }
            }
            int idx = annotObject.Idx;
            annotObject.Set(annotObject.Idx,
                            this.inpName.text,
                            CurAngle(),
                            annotObject.CreatedTime,
                            ProjCtrl.Now(),
                            this.SceneName);
            ProjCtrl.LinkAnnot(MainCtrl.CurScene, this.TargetObj);

            this.AnnotView.GetComponent<LinkAnnotViewCtrl>().BuildList();
            this.AnnotView.GetComponent<LinkAnnotViewCtrl>().GetObject(idx).
                GetComponent<LinkAnnotObjCtrl>().OnCollided(true);
        }
        LinkAnnotObjCtrl.SetLock(false);

        if (this.TargetObj != null)
        {
            this.TargetObj.transform.parent = null;
            this.TargetObj.SetActive(false);
            DestroyImmediate(this.TargetObj);
            this.TargetObj = null;
        }

        this.AnnotListView.GetComponent<AnnotListCtrl>().RebuildList();
        PanoramicViewCtrl.Raycast();

        this.gameObject.SetActive(false);
    }

    /**
     * @brief 어노테이션 삭제 버튼 클릭
     */
    void OnDelButtonClicked()
    {
        if (this.TargetObj == null) return;

        if (this.TextAnnotContentEditView.activeSelf)
        {
            ProjCtrl.DelTextAnnot(MainCtrl.CurScene, this.TargetObj.GetComponent<TextAnnotObjCtrl>().Idx);

            this.TextAnnotView.GetComponent<TextAnnotViewCtrl>().BuildList();
        }
        else if (this.ImageAnnotContentEditView.activeSelf)
        {
            ProjCtrl.DelImageAnnot(MainCtrl.CurScene, this.TargetObj.GetComponent<ImageAnnotObjCtrl>().Idx);

            this.ImageAnnotView.GetComponent<ImageAnnotViewCtrl>().BuildList();
        }
        else if (this.LinkAnnotContentEditView.activeSelf)
        {
            ProjCtrl.DelLinkAnnot(MainCtrl.CurScene, this.TargetObj.GetComponent<LinkAnnotObjCtrl>().Idx);

            this.AnnotView.GetComponent<LinkAnnotViewCtrl>().BuildList();
        }
        this.inpName.readOnly = false;
        LinkAnnotObjCtrl.SetLock(false);

        this.AnnotListView.GetComponent<AnnotListCtrl>().RebuildList();

        this.gameObject.SetActive(false);
    }

    /**
     * @brief 어노테이션 이름 입력 뷰 DONE 버튼 클릭
     * 대상 오브젝트에 값 설정
     */
    void OnNameInputEndEdit()
    {
        if (this.TextAnnotContentEditView.activeSelf) 
        {
            this.TargetObj.GetComponent<TextAnnotObjCtrl>().Name = this.inpName.text;
            this.TargetObj.GetComponent<TextAnnotObjCtrl>().OnEdit = false;
            this.TargetObj.GetComponent<TextAnnotObjCtrl>().OnCollided(false);
            this.TargetObj.GetComponent<TextAnnotObjCtrl>().OnEdit = true;
        } 
        else if (this.ImageAnnotContentEditView.activeSelf)
        {
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().Name = this.inpName.text;
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnEdit = false;
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnCollided(false);
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnEdit = true;
        }
        else if (this.LinkAnnotContentEditView.activeSelf)
        {
            this.TargetObj.GetComponent<LinkAnnotObjCtrl>().Name = this.inpName.text;
            this.TargetObj.GetComponent<LinkAnnotObjCtrl>().SceneName = this.inpName.text;
            this.TargetObj.GetComponent<LinkAnnotObjCtrl>().OnCollided(false);
            this.SceneName = this.inpName.text;
        }
        PanoramicViewCtrl.Raycast();
    }

    /**
     * @brief 텍스트 어노테이션 내용 입력 뷰 DONE 버튼 클릭
     * 대상 오브젝트에 값 설정
     */
    void OnDescInputEndEdit()
    {
        this.TargetObj.GetComponent<TextAnnotObjCtrl>().Desc = this.inpDesc.text;
        this.TargetObj.GetComponent<TextAnnotObjCtrl>().OnEdit = false;
        this.TargetObj.GetComponent<TextAnnotObjCtrl>().OnCollided(false);
        this.TargetObj.GetComponent<TextAnnotObjCtrl>().OnEdit = true;
        PanoramicViewCtrl.Raycast();
    }

    /**
     * @brief 텍스트 어노테이션 내용 표시 위치 선택 버튼 클릭
     * 값에 해당하는 이미지를 버튼에 적용, 대상 오브젝트에 값 설정
     */
    void OnPosButtonClicked()
    {
        this.Pos = (this.Pos + 1) % 5;
        this.TargetObj.GetComponent<TextAnnotObjCtrl>().Pos = PosToString(this.Pos);
        this.btnPos.GetComponent<Image>().sprite = PosToSprite(this.Pos);
       
        this.TargetObj.GetComponent<TextAnnotObjCtrl>().OnEdit = false;
        this.TargetObj.GetComponent<TextAnnotObjCtrl>().OnCollided(false);
        this.TargetObj.GetComponent<TextAnnotObjCtrl>().OnEdit = true;
        PanoramicViewCtrl.Raycast();
    }

    /**
     * @brief 이미지 설정 버튼 클릭
     * 이미지 피커 뷰 열기
     */
    void OnImageButtonClicked()
    {
        LoadTextureFromImagePicker.ShowPhotoLibrary(gameObject.name, "OnFinishedImagePicker");
    }

    /**
     * @brief 이미지 설정 버튼 클릭 콜백
     * 임시 경로에 이미지 저장 후 기존 이미지 설정 함수 콜
     * 이미지 어노테이션용 이미지는 크기를 1/10로 줄임
     */
    void OnFinishedImagePicker(string message)
    {
        if (LoadTextureFromImagePicker.IsLoaded())
        {
            EffectViewCtrl.Progress(true);
            int width = LoadTextureFromImagePicker.GetLoadedTextureWidth();
            int height = LoadTextureFromImagePicker.GetLoadedTextureHeight();
            if (this.ImageAnnotContentEditView.activeSelf)
            {
                width /= 10;
                height /= 10;
            }
            bool mipmap = true;
            Texture2D texture = LoadTextureFromImagePicker.GetLoadedTexture(message, width, height, mipmap);
            if (texture)
            {
                string path = Path.Combine(MainCtrl.TEMP_DIR, ProjCtrl.DateTime() + ".JPG");
                LoadTextureFromImagePicker.SaveToLocalFile(path, texture);
                this.OnImageSelect(path, ImageOrientation.UP);
            }
            LoadTextureFromImagePicker.ReleaseLoadedImage();
        }
        else
        {
            LoadTextureFromImagePicker.Release();
        }
    }
  
    /**
     * @brief 이미지 피커 뷰에서 이미지를 선택
     * 대상 오브젝트에 값 설정
     */
    void OnImageSelect(string imgPath, ImageAndVideoPicker.ImageOrientation imgOrientation)
    {
        string imgName = Path.GetFileName(imgPath);
        string path = Path.Combine(MainCtrl.TEMP_DIR, imgName);
      
        if (this.ImageAnnotContentEditView.activeSelf)
        {
            this.ImagePath = imgName;
            this.ImageRotation = 0;
            this.SetImageTexture(ImageAnnotObjCtrl.LoadTexture(path, -this.ImageRotation));
            this.imgImage.color = Color.white;

            if (this.TargetObj)
            {
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().ImagePath = this.ImagePath;
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().ImageRotation = this.ImageRotation;
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().SetImage(
                    ImageAnnotObjCtrl.LoadTexture(this.ImagePath, this.ImageRotation));
                
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnEdit = false;
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnCollided(false);
                this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnEdit = true;
                PanoramicViewCtrl.Raycast();
            }
        }
        else if (this.LinkAnnotContentEditView.activeSelf)
        {
            if (PanoramicViewCtrl.isPanoramic(path))
            {
                this.SceneImagePath = imgName;
                this.SetLinkTexture(ImageAnnotObjCtrl.LoadTexture(path));
                this.imgLink.color = Color.white;    
            }else{
                //IOSPicker.BrowseImage(true);
                LoadTextureFromImagePicker.ShowPhotoLibrary(gameObject.name, "OnFinishedImagePicker");
            }
        }
        EffectViewCtrl.Progress(false);
    }

    /**
     * @brief 이미지 반 시계방향 회전 버튼 클릭
     * 이미지 어노테이션의 이미지를 반 시계방향으로 회전 
     */
    void OnLRotateButtonClicked()
    {
        this.ImageRotation = this.ImageRotation - 90;
        if (this.ImageRotation < 0) this.ImageRotation = 360 + this.ImageRotation;
        this.SetImageTexture(ImageAnnotObjCtrl.RotateTexture(this.imgImage.texture as Texture2D, false));
        this.imgImage.color = Color.white;

        if (this.TargetObj)
        {
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().ImagePath = this.ImagePath;
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().ImageRotation = this.ImageRotation;
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().SetImage(
                ImageAnnotObjCtrl.LoadTexture(this.ImagePath, this.ImageRotation));

            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnEdit = false;
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnCollided(false);
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnEdit = true;
            PanoramicViewCtrl.Raycast();
        }
    }

    /**
     * @brief 이미지 시계방향 회전 버튼 클
     * 이미지 어노테이션의 이미지를 시계방향으로 회전 
     */
    void OnRRotateButtonClicked()
    {
        this.ImageRotation = this.ImageRotation + 90;
        if (270 < this.ImageRotation) this.ImageRotation = 360 - this.ImageRotation;
        this.SetImageTexture(ImageAnnotObjCtrl.RotateTexture(this.imgImage.texture as Texture2D, true));
        this.imgImage.color = Color.white;

        if (this.TargetObj)
        {
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().ImagePath = this.ImagePath;
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().ImageRotation = this.ImageRotation;
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().SetImage(
                ImageAnnotObjCtrl.LoadTexture(this.ImagePath, this.ImageRotation));

            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnEdit = false;
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnCollided(false);
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnEdit = true;
            PanoramicViewCtrl.Raycast();
        }
    }

    /**
     * @brief 링크 어노테이션 이미지 버튼 클릭
     * 이미지 피커 열기
     */
    void OnLinkButtonClicked()
    {
        if (this.inpName.readOnly) return;

        LoadTextureFromImagePicker.ShowPhotoLibrary(gameObject.name, "OnFinishedImagePicker");
    }

    /**
     * @brief 이전 씬 보기 버튼 클릭
     * 현재 씬에서 한단계 이전 씬을 보여줌 
     */
    void OnLSceneButtonClicked()
    {
        if (this.SceneIdx == 0) return;

        this.SceneIdx -= 1;

        this.SceneName = ProjCtrl.SceneName(this.SceneIdx);
        this.inpName.text = this.SceneName;
        this.inpName.readOnly = true;

        this.SceneImagePath = ProjCtrl.SceneImage(this.SceneIdx);
        this.SetLinkTexture(ImageAnnotObjCtrl.LoadTexture(this.SceneImagePath));
        this.imgLink.color = Color.white;
    }

    /**
     * @brief 다음 씬 보기 버튼 클릭
     * 현재 씬에서 한단계 다음 씬을 보여줌 
     */
    void OnRSceneButtonClicked()
    {
        if (this.SceneIdx == ProjCtrl.SceneCount()) return;

        this.SceneIdx += 1;

        if (this.SceneIdx == ProjCtrl.SceneCount())
        {
            this.SceneName = "";
            this.inpName.text = this.SceneName;
            this.inpName.readOnly = false;

            this.SceneImagePath = "";
            this.SetLinkTexture(null);
            this.imgLink.color = Color.clear;
        }
        else
        {
            this.SceneName = ProjCtrl.SceneName(this.SceneIdx);
            this.inpName.text = this.SceneName;
            this.inpName.readOnly = true;

            this.SceneImagePath = ProjCtrl.SceneImage(this.SceneIdx);
            this.SetLinkTexture(ImageAnnotObjCtrl.LoadTexture(this.SceneImagePath));
            this.imgLink.color = Color.white;
        }
    }

    //PRIVATE
    /**
     * @brief 현재 조준된 파노라믹 뷰 위의 위치 값을 리턴 
     * @returns 위치 값, 0 <= x < 360 -90 <= y <= 90 
     */
    Vector3 CurAngle()
    {
        return ProjCtrl.ToAngleRange(PanoramicViewCtrl.Angle());
    }

    /**
     * @brief 텍스트 어노테이션의 위치 문자열에 해당하는 값을 리턴
     * @param _str 텍스트 어노테이션 내용 표시 위치 문자열
     * @returns 텍스트 어노테이션 내용 표시 위치 값
     */
    int StringToPos(string _str)
    {
        if (_str.ToLower() == TextAnnotContentObjCtrl.LTPOS) return 0;
        else if (_str.ToLower() == TextAnnotContentObjCtrl.RTPOS) return 1;
        else if (_str.ToLower() == TextAnnotContentObjCtrl.LBPOS) return 2;
        else if (_str.ToLower() == TextAnnotContentObjCtrl.RBPOS) return 3;
        else if(_str.ToLower() == TextAnnotContentObjCtrl.CLPOS) return 4;
            
        return 4;
    }

    /**
     * @brief 텍스트 어노테이션의 위치 값에 해당하는 문자열 값을 리턴
     * @param _pos 텍스트 어노테이션 내용 표시 위치 값, 0 ~ 4
     * @returns 텍스트 어노테이션 내용 표시 위치 문자열
     */
    string PosToString(int _pos)
    {
        switch (_pos)
        {
            case 0: return TextAnnotContentObjCtrl.LTPOS;
            case 1: return TextAnnotContentObjCtrl.RTPOS;
            case 2: return TextAnnotContentObjCtrl.LBPOS;
            case 3: return TextAnnotContentObjCtrl.RBPOS;
            case 4: return TextAnnotContentObjCtrl.CLPOS;
            default: return TextAnnotContentObjCtrl.CLPOS;
        }
    }

    /**
     * @brief 텍스트 어노테이션의 위치 값에 해당하는 버튼 이미지를 리턴
     * @param _pos 텍스트 어노테이션 내용 표시 위치 값, 0 ~ 4
     * @returns 텍스트 어노테이션 내용 표시 위치 버튼 이미지
     */
    Sprite PosToSprite(int _pos)
    {
        switch (_pos)
        {
            case 0: return LTPOS;
            case 1: return RTPOS;
            case 2: return LBPOS;
            case 3: return RBPOS;
            case 4: return CLPOS;
            default: return CLPOS;
        }
    }

    /**
     * @brief 어노테이션 추가 뷰 활성화
     * @param _key 추가 할 어노테이션 오브젝트의 종류, {TEXT_KEY, IMAGE_KEY}
     */
    void NewAnnot(string _key)
    {
        if (this.TargetObj)
        {
            this.ImageAnnotView.GetComponent<ImageAnnotViewCtrl>().HideAnnotContent();
            this.TextAnnotView.GetComponent<TextAnnotViewCtrl>().HideAnnotContent();
            this.TargetObj.transform.parent = null;
            this.TargetObj.SetActive(false);
            DestroyImmediate(this.TargetObj);
        }

        this.TargetObj = null;
        this.OriAngle = Vector3.negativeInfinity;
        Vector3 curAngle = CurAngle();

        this.TextAnnotContentEditView.SetActive(false);
        this.ImageAnnotContentEditView.SetActive(false);
        this.LinkAnnotContentEditView.SetActive(false);

        this.inpName.text = "";
        this.OriName = "";
        this.txtAngle.text = AnnotListCtrl.AngleToString(curAngle);
        if (_key == AnnotListCtrl.TEXT_KEY)
        {
            this.TargetObj = ProjCtrl.NewTextAnnot();
            this.TargetObj.GetComponent<TextAnnotObjCtrl>().OnEdit = false;
            this.TargetObj.GetComponent<TextAnnotObjCtrl>().Idx = this.TextAnnotView.GetComponent<TextAnnotViewCtrl>().NewIdx();
            this.TargetObj.GetComponent<TextAnnotObjCtrl>().CreatedTime = ProjCtrl.Now();
            this.TargetObj.GetComponent<TextAnnotObjCtrl>().Angle = curAngle;

            this.TargetObj.GetComponent<TextAnnotObjCtrl>().HideAnnotContent =
                    this.TextAnnotView.GetComponent<TextAnnotViewCtrl>().HideAnnotContent;
            this.TargetObj.GetComponent<TextAnnotObjCtrl>().ShowAnnotContent =
                this.TextAnnotView.GetComponent<TextAnnotViewCtrl>().ShowAnnotContent;

            this.Pos = this.StringToPos(TextAnnotContentObjCtrl.CLPOS);
            this.OriPos = TextAnnotContentObjCtrl.CLPOS;
            this.btnPos.GetComponent<Image>().sprite = PosToSprite(this.Pos);
            this.inpDesc.text = "";
            this.OriDesc = "";
            this.TextAnnotContentEditView.SetActive(true);
        }
        else if (_key == AnnotListCtrl.IMAGE_KEY)
        {
            this.TargetObj = ProjCtrl.NewImageAnnot();
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().OnEdit = true;
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().Idx = this.ImageAnnotView.GetComponent<ImageAnnotViewCtrl>().NewIdx();
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().CreatedTime = ProjCtrl.Now();
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().Angle = curAngle;

            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().HideAnnotContent =
                    this.ImageAnnotView.GetComponent<ImageAnnotViewCtrl>().HideAnnotContent;
            this.TargetObj.GetComponent<ImageAnnotObjCtrl>().ShowAnnotContent =
                    this.ImageAnnotView.GetComponent<ImageAnnotViewCtrl>().ShowAnnotContent;

            this.ImagePath = "";
            this.OriImagePath = "";
            this.ImageRotation = 0;
            this.OriImageRotation = 0;
            this.SetImageTexture(null);
            //this.imgImage.color = Color.clear;
            this.ImageAnnotContentEditView.SetActive(true);
        }
        else if (_key == AnnotListCtrl.LINK_KEY)
        {
            this.TargetObj = ProjCtrl.NewLinkAnnot();
            this.TargetObj.GetComponent<LinkAnnotObjCtrl>().Idx = this.AnnotView.GetComponent<LinkAnnotViewCtrl>().NewIdx();
            this.TargetObj.GetComponent<LinkAnnotObjCtrl>().CreatedTime = ProjCtrl.Now();
            this.TargetObj.GetComponent<LinkAnnotObjCtrl>().Angle = curAngle;

            this.TargetObj.GetComponent<LinkAnnotObjCtrl>().MoveToScene = 
                this.AnnotView.GetComponent<LinkAnnotViewCtrl>().MoveToScene;

            this.inpName.readOnly = false;
                
            this.SceneIdx = ProjCtrl.SceneCount();
            //this.OriSceneIdx = ProjCtrl.SceneCount();
            this.SceneName = "";
            this.SceneImagePath = "";
            this.SetLinkTexture(null);
            this.imgLink.color = Color.clear;
            this.LinkAnnotContentEditView.SetActive(true);
        }
        LinkAnnotObjCtrl.SetLock(true);
        PanoramicViewCtrl.Set(this.TargetObj, curAngle);
        PanoramicViewCtrl.Raycast();

        this.btnTextAnnot.gameObject.SetActive(true);
        this.btnImageAnnot.gameObject.SetActive(true);
        this.btnLinkAnnot.gameObject.SetActive(true);
        this.EditMenuView.SetActive(false);
    }

    /**
     * @brief 어노테이션 편집 뷰 활성화
     * @param _key 편집 할 어노테이션 오브젝트의 종류, {TEXT_KEY, IMAGE_KEY}
     * @param _obj 편집 할 어노테이션 오브젝트의 GameObject 인스턴스
     */
    void EditAnnot(string _key, GameObject _obj)
    {
        if (_obj == null) return;

        this.TargetObj = _obj;

        this.TextAnnotContentEditView.SetActive(false);
        this.ImageAnnotContentEditView.SetActive(false);
        this.LinkAnnotContentEditView.SetActive(false);

        if (_key == AnnotListCtrl.TEXT_KEY)
        {
            TextAnnotObjCtrl annotObject = this.TargetObj.GetComponent<TextAnnotObjCtrl>();
            annotObject.OnEdit = true;

            this.OriAngle = annotObject.Angle;
            this.OriName = annotObject.Name;
            this.OriPos = annotObject.Pos;
            this.OriDesc = annotObject.Desc;

            this.inpName.text = annotObject.Name;
            this.txtAngle.text = AnnotListCtrl.AngleToString(annotObject.Angle);
            this.Pos = this.StringToPos(annotObject.Pos);
            this.btnPos.GetComponent<Image>().sprite = PosToSprite(this.Pos);
            this.inpDesc.text = annotObject.Desc;

            this.TextAnnotContentEditView.SetActive(true);
        }
        else if (_key == AnnotListCtrl.IMAGE_KEY)
        {
            ImageAnnotObjCtrl annotObject = this.TargetObj.GetComponent<ImageAnnotObjCtrl>();
            annotObject.OnEdit = true;

            this.OriAngle = annotObject.Angle;
            this.OriName = annotObject.Name;
            this.OriImagePath = annotObject.ImagePath;
            this.OriImageRotation = annotObject.ImageRotation;

            this.inpName.text = annotObject.Name;
            this.txtAngle.text = AnnotListCtrl.AngleToString(annotObject.Angle);
            this.ImagePath = annotObject.ImagePath;
            this.ImageRotation = annotObject.ImageRotation;
            this.SetImageTexture(ImageAnnotObjCtrl.LoadTexture(this.ImagePath, this.ImageRotation));
            this.imgImage.color = Color.white;

            this.ImageAnnotContentEditView.SetActive(true);
        }
        else if (_key == AnnotListCtrl.LINK_KEY)
        {
            LinkAnnotObjCtrl annotObject = this.TargetObj.GetComponent<LinkAnnotObjCtrl>();
            this.OriAngle = annotObject.Angle;

            this.inpName.text = annotObject.Name;
            this.inpName.readOnly = true;
            this.txtAngle.text = AnnotListCtrl.AngleToString(annotObject.Angle);

            this.SceneIdx = ProjCtrl.SceneIdx(annotObject.SceneName);
            //this.OriSceneIdx = this.SceneIdx;
            this.SceneName = annotObject.SceneName;
            this.SceneImagePath = ProjCtrl.SceneImage(this.SceneIdx);
            this.SetLinkTexture(ImageAnnotObjCtrl.LoadTexture(this.SceneImagePath));
            this.imgLink.color = Color.white;

            this.LinkAnnotContentEditView.SetActive(true);
        }
        LinkAnnotObjCtrl.SetLock(true);

        this.btnTextAnnot.gameObject.SetActive(false);
        this.btnImageAnnot.gameObject.SetActive(false);
        this.btnLinkAnnot.gameObject.SetActive(false);
        this.EditMenuView.SetActive(true);
    }

    /**
     * @brief 이미지 어노테이션의 이미지 뷰에 텍스쳐 설정
     * @param _texture 새 텍스쳐
     * 현재 설정된 텍스쳐를 삭제하고 설정함
     */
    void SetImageTexture(Texture2D _texture)
    {
        Texture texture = null;
        if (this.imgImage.texture != null) 
        {
            texture = this.imgImage.texture;
        }
        this.imgImage.texture = _texture;
        if (texture != null && !texture.Equals(_texture))
        {
            Destroy(texture);
        }
    }

    /**
     * @brief 링크 어노테이션의 이미지 뷰에 텍스쳐 설정
     * @param _texture 새 텍스쳐
     * 현재 설정된 텍스쳐를 삭제하고 설정함
     */
    void SetLinkTexture(Texture2D _texture)
    {
        Texture texture = null;
        if (this.imgLink.texture != null)
        {
            texture = this.imgLink.texture;
        }
        this.imgLink.texture = _texture;
        if (texture != null && !texture.Equals(_texture))
        {
            Destroy(texture);
        }
    }

    //PUBLIC
    /**
     * @brief 텍스트 어노테이션 추가
     */
    public void NewTextAnnot()
    {
        this.NewAnnot(AnnotListCtrl.TEXT_KEY);
    }

    /**
     * @brief 이미지 어노테이션 추가
     */
    public void NewImageAnnot()
    {
        this.NewAnnot(AnnotListCtrl.IMAGE_KEY);
    }

    /**
     * @brief 텍스트 어노테이션 편집 뷰 활성화
     * @param _obj 편집 할 TextAnnotObj의 GameObject 인스턴스
     */
    public void EditTextAnnot(GameObject _obj)
    {
        this.EditAnnot(AnnotListCtrl.TEXT_KEY, _obj);
    }

    /**
     * @brief 이미지 어노테이션 편집 뷰 활성화
     * @param _obj 편집 할 ImageAnnotObj의 GameObject 인스턴스
     */
    public void EditImageAnnot(GameObject _obj)
    {
        this.EditAnnot(AnnotListCtrl.IMAGE_KEY, _obj);
    }

    /**
     * @brief 링크 어노테이션 편집 뷰 활성화
     * @param _obj 편집 할 LinkAnnotObj의 GameObject 인스턴스
     */
    public void EditLinkAnnot(GameObject _obj)
    {
        this.EditAnnot(AnnotListCtrl.LINK_KEY, _obj);
    }

    /**
     * @brief 어노테이션 리스트 빌드
     */
    public void BuildList()
    {
        this.TextAnnotView.GetComponent<TextAnnotViewCtrl>().BuildList();
        this.ImageAnnotView.GetComponent<ImageAnnotViewCtrl>().BuildList();
        this.AnnotView.GetComponent<LinkAnnotViewCtrl>().BuildList();
    }
}
