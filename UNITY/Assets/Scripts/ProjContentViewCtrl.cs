﻿/**
 * @file ProjContentViewCtrl.cs 
 * @date 2018/07/23 
 * @author KIMJISOO(kind.jamie@gmail.com)
 * @brief 프로젝트 컨텐츠 뷰 컨트롤 스크립트
 */
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using AlertView.iOS;
using System.Collections.Generic;

/**
 * @class ProjContentViewCtrl
 * @date 2018/07/23
 * @brief 프로젝트 컨텐츠 뷰 컨트롤
 * ProjContentView의 컨트롤
 */
public class ProjContentViewCtrl : MonoBehaviour {

    //EXTERNAL VIEW
    public GameObject ProjListView;         /** 프로젝트 리스트 뷰 */
    public GameObject AnnotListView;        /** 어노테이션 리스트 뷰 */
    public GameObject AnnotEditView;        /** 어노테이션 편집 뷰 */
    //EXTERNAL VIEW

    //SINGLETON
    static public ProjContentViewCtrl Instance = null;    /** 싱글턴 인스턴스 */

    //Menu
    public Button btnAdd;                   /** 어노테이션 추가 버튼 */

    public GameObject ProjCtrlMenuView;   
    public Text txtName;                    /** 프로젝트 이름 필드 */
    public Button btnBack;                  /** 닫기 버튼 */
    public Button btnEdit;                  /** 편집 버튼 */
    public Button btnList;                  /** 리스트 버튼 */

    public GameObject ProjEditView;         /** 프로젝트 편집 뷰 */
    public InputField inpName;              /** 프로젝트 이름 입력 필드 */
    public Button btnCancel;                /** 취소 버튼 */
    public Button btnDone;                  /** 확인 버튼 */

    /**
     * @brief 앱 구동시, 싱글턴 인스턴스 설정
     */
    void Awake()
    {
        if (Instance == null) Instance = this;
    }

    /**
     * @brief 뷰와 필드 초기화, 리스너 등록
     */
    void Start () 
    {
        this.btnAdd.onClick.AddListener(OnAddButtonClicked);

        this.btnBack.onClick.AddListener(OnBackButtonClicked);
        this.btnEdit.onClick.AddListener(OnEditButtonClicked);
        this.btnCancel.onClick.AddListener(OnCancelButtonClicked);
        this.btnDone.onClick.AddListener(OnDoneButtonClicked);

        this.btnList.onClick.AddListener(OnListButtonClicked);

        this.gameObject.SetActive(false);
        this.ProjEditView.SetActive(false);
        this.ProjCtrlMenuView.SetActive(false);

        this.AnnotListView.SetActive(false);
        this.AnnotEditView.SetActive(false);

        PanoramicViewCtrl.SetActive(false);
	}

    /**
     * @brief 로딩 이펙트 해제
     */
    void OnEnable()
    {
        EffectViewCtrl.Progress(false);
    }

    //DELEGATE
    /**
     * @brief 어노테이션 추가 버튼 클릭 
     */
    void OnAddButtonClicked()
    {
        if (this.ProjEditView.activeSelf) return;

        this.AnnotEditView.SetActive(true);
        this.AnnotEditView.GetComponent<AnnotEditViewCtrl>().NewTextAnnot();
    }

    /**
     * @brief 닫기 버튼 클릭 
     */
    void OnBackButtonClicked()
    {
        if (this.ProjEditView.activeSelf) return;
        if (this.AnnotEditView.activeSelf) return;
        if (PanoramicViewCtrl.OnThread()) return;

        if (ProjCtrl.IsEdit())
        {
            ShowAlertView();   
        }
        else 
        {
            ProjCtrl.CloseProject();
            this.CloseProjContentView();
        }
    }

    /**
     * @brief 프로젝트 저장 선택창 열기
     */
    void ShowAlertView()
    {
        var alert = new AlertData(
            ProjCtrl.ProjectName(),
            "WANT TO SAVE YOUR CHANGES BEFORE CLOSE?",
            AlertStyle.ActionSheet,
            new List<ButtonData>()
            {
                new ButtonData("YES", ButtonStyle.Default, CallbackHandlerSave),
                new ButtonData("NO", ButtonStyle.Destructive, CallbackHandlerNo),
                new ButtonData("CANCEL", ButtonStyle.Cancel, CallbackHandlerCancel)
            }
        );
        
        AlertViewManager.ShowAlertView(alert);
    }

    /**
     * @brief 저장하고 닫기 버튼 클릭 
     */
    void CallbackHandlerSave()
    {
        ProjCtrl.SaveProject();
        this.CloseProjContentView();
    }

    /**
     * @brief 저장하지 않고 닫기 버튼 클릭 
     */
    void CallbackHandlerNo()
    {
        ProjCtrl.CloseProject();
        this.CloseProjContentView();
    }

    /**
     * @brief 닫기취소 버튼 클릭 
     */
    void CallbackHandlerCancel()
    {
        //do nothing
    }

    /**
     * @brief 프로젝트 컨텐츠 뷰 닫기
     */
    void CloseProjContentView()
    {
        PanoramicViewCtrl.SetActive(false);
        this.gameObject.SetActive(false);
        this.AnnotEditView.SetActive(false);

        this.ProjListView.GetComponent<ProjListCtrl>().BuildList();
        this.ProjListView.SetActive(true);
    }

    /**
     * @brief 편집 버튼 클릭 
     */
    void OnEditButtonClicked()
    {
        if (this.ProjEditView.activeSelf) return;
        if (this.AnnotListView.activeSelf) return;
        if (this.AnnotEditView.activeSelf) return;

        this.ProjEditView.SetActive(true);

        this.inpName.text = ProjCtrl.ProjectName();
        this.inpName.ActivateInputField();
    }

    /**
     * @brief 저장 버튼 클릭 
     */
    void OnSaveButtonClicked()
    {
        if (this.ProjEditView.activeSelf) return;
        if (this.AnnotEditView.activeSelf) return;
        if (PanoramicViewCtrl.OnThread()) return;

        ProjCtrl.SaveProject();

        PanoramicViewCtrl.SetActive(false);
        this.gameObject.SetActive(false);
        this.AnnotEditView.SetActive(false);

        this.ProjListView.GetComponent<ProjListCtrl>().BuildList();
        this.ProjListView.SetActive(true);
    }

    /**
     * @brief 리스트 버튼 클릭 
     */
    void OnListButtonClicked()
    {
        if (this.ProjEditView.activeSelf) return;

        this.AnnotListView.SetActive(!this.AnnotListView.activeSelf);
    }

    /**
     * @brief 프로젝트 편집 취소 버튼 클릭 
     */
    void OnCancelButtonClicked()
    {
        this.ProjEditView.SetActive(false);
    }

    /**
     * @brief 프로젝트 편집 확인 버튼 클릭 
     */
    void OnDoneButtonClicked()
    {
        this.ProjEditView.SetActive(false);
        if (this.inpName.text.Length == 0 || this.inpName.text == ProjCtrl.ProjectName()) return;

        ProjCtrl.ProjectName(this.inpName.text);
        this.ProjName(ProjCtrl.ProjectName());
    }

    /**
     * @brief 프로젝트 열기
     * @param _sceneIdx
     */
    void _Open(int _sceneIdx, System.Action _callback)
    {
        Debug.Log("Open Scene " + _sceneIdx.ToString());

        MainCtrl.CurScene = _sceneIdx;

        PanoramicViewCtrl.SetActive(true);
        PanoramicViewCtrl.ClearAnnots();
        PanoramicViewCtrl.SetTexture(Path.Combine(MainCtrl.TEMP_DIR,
                                                  ProjCtrl.SceneImage(MainCtrl.CurScene)), 
                                     OnEndSetTexture,
                                     _callback);
    }

    /**
     * @brief 텍스쳐 설정 종료, 콜백함수
     */
    void OnEndSetTexture()
    {
        if (this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(false);
            this.ProjEditView.SetActive(false);
            this.ProjCtrlMenuView.SetActive(false);

            this.AnnotListView.SetActive(false);
            this.AnnotEditView.SetActive(false);

            PanoramicViewCtrl.SetActive(false);
        }

        EffectViewCtrl.FadeIn();
        PanoramicViewCtrl.SetActive(true);

        this.ProjName(ProjCtrl.ProjectName());

        this.ProjCtrlMenuView.SetActive(true);
        this.ProjEditView.SetActive(false);
        this.AnnotListView.SetActive(false);
        this.AnnotEditView.SetActive(false);
        this.gameObject.SetActive(true);

        this.AnnotEditView.GetComponent<AnnotEditViewCtrl>().BuildList();
    }

    /**
     * @brief 프로젝트 이름 필드 설정
     * @param _name 프로젝트 이름
     * 프로젝트 이름 뒤에 Scene 이름을 추가
     */
    void ProjName(string _name)
    {
        this.txtName.text = _name + " - " + ProjCtrl.SceneName(MainCtrl.CurScene);
    }

    //STATIC
    /**
     * @brief 프로젝트 열기
     * @param _sceneIdx
     */
    static public void Open(int _sceneIdx = 0, System.Action _callback = null)
    {
        ProjContentViewCtrl.Instance._Open(_sceneIdx, _callback);
    }
}
