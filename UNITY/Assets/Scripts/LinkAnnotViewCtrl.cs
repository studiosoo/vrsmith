﻿/**
* @file LinkAnnotViewCtrl.cs
* @date 2018/08/10
* @author KIMJISOO(kind.jamie@gmail.com)
* @brief 링크 어노테이션 뷰 컨트롤 스크립트
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
* @class LinkAnnotViewCtrl
* @date 2018/08/10
* @brief 링크 어노테이션 뷰 컨트롤
* 링크 어노테이션의 리스트
*/
public class LinkAnnotViewCtrl : MonoBehaviour {

    //OBJECTS
    List<GameObject> Annots;        /** 링크 어노테이션 오브젝트 리스트 */

    /**
     * @brief 활성화시, 리스트 갱신
     */
    void OnEnable()
    {
        this.Annots = new List<GameObject>();
    }

    //PUBLIC
    /**
     * @brief 이미지 어노테이션 오브젝트 리스트 리턴
     * @returns 게임 오브젝트의 리스트
     */
    public List<GameObject> List()
    {
        return this.Annots;
    }

    /**
     * @brief 인덱스에 해당하는 게임 오브젝트 리턴
     * @param _idx 어노테이션 오브젝트 리스트의 인덱스, 0~
     * @returns 게임 오브젝트
     */
    public GameObject GetObject(int _idx)
    {
        return this.Annots[_idx];
    }

    /**
     * @brief 어노테이션 오브젝트 리스트의 새 인덱스 리턴
     * @returns 인덱스 , 0~
     */
    public int NewIdx()
    {
        if (Annots.Count == 0) return 0;
        GameObject lastObj = Annots[Annots.Count - 1];
        return lastObj.GetComponent<LinkAnnotObjCtrl>().Idx + 1;
    }

    /**
     * @brief 어노테이션 오브젝트 리스트 갱신
     * @returns 인덱스 , 0~
     */
    public void BuildList()
    {
        if (this.Annots != null)
        {
            foreach (GameObject obj in this.Annots)
            {
                Destroy(obj);
            }
            this.Annots.Clear();
        }

        this.Annots = ProjCtrl.LinkAnnots(MainCtrl.CurScene);

        foreach (GameObject obj in this.Annots)
        {
            LinkAnnotObjCtrl annotObject = obj.GetComponent<LinkAnnotObjCtrl>();
            if (annotObject)
            {
                annotObject.MoveToScene = this.MoveToScene;
                PanoramicViewCtrl.Set(obj, annotObject.Angle);
                PanoramicViewCtrl.Set(obj, annotObject.Angle);
            }
        }
    }

    /**
     * @brief 
     * @param _scene
     */
    public void MoveToScene(string _scene)
    {
        Debug.Log("MoveToScene " + _scene);

        int idx = 0;
        for (; idx < ProjCtrl.SceneCount(); idx++)
        {
            if (_scene == ProjCtrl.SceneName(idx)) break;
        }

        EffectViewCtrl.Progress(true);

        ProjContentViewCtrl.Open(idx);
    }

}
