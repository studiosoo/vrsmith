/**
* @file TextAnnotContentObjCtrl.cs
* @date 2018/07/23
* @author KIMJISOO(kind.jamie@gmail.com)
* @brief 텍스트 어노테이션 컨텐츠 오브젝트 컨트롤 스크립트
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
* @class TextAnnotContentObjCtrl
* @date 2018/07/23
* @brief 텍스트 어노테이션 컨텐츠 오브젝트 컨트롤
* TextAnnotContentObj의 컨트롤
*/
public class TextAnnotContentObjCtrl : MonoBehaviour {

    //DEFINE
    static public string LTPOS = "left-top";        /** 표시 위치 Left-Top */
    static public string RTPOS = "right-top";       /** 표시 위치 Right-Top */
    static public string LBPOS = "left-bottom";     /** 표시 위치 Left-Bottom */
    static public string RBPOS = "right-bottom";    /** 표시 위치 Right-Bottom */
    static public string CLPOS = "cloud";           /** 표시 위치 말풍선 */
    //DEFINE

    public Text txtName;        /** 어노테이션 이름 표시 필드 */
    public Text txtDesc;        /** 어노테이션 설명 표시 필드 */

    /**
     * @brief 어노테이션 내용 표시
     * @param _name 어노테이션 이름
     * @param _desc 어노테이션 설명
     */
    public void Set(string _name, string _desc)
    {
        this.txtName.text = _name;
        this.txtDesc.text = _desc;
    }
}
