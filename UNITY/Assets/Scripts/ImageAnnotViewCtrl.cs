/**
* @file ImageAnnotViewCtrl.cs
* @date 2018/07/23
* @author KIMJISOO(kind.jamie@gmail.com)
* @brief 이미지 어노테이션 뷰 컨트롤 스크립트
*/
using System.Collections.Generic;
using UnityEngine;

/**
* @class ImageAnnotViewCtrl
* @date 2018/07/23
* @brief 이미지 어노테이션 뷰 컨트롤
* ImageAnnotContentView의 컨트롤과 이미지 어노테이션의 리스트
*/
public class ImageAnnotViewCtrl : MonoBehaviour {
    
    //CONTENT
    public GameObject ContentView;          /** 이미지 어노테이션 컨텐츠 뷰 */

    //OBJECTS
    List<GameObject> Annots = null;         /** 이미지 어노테이션 오브젝트 리스트 */
    GameObject TargetObj = null;            /** 컨텐츠 표시 대상 어노테이션 오브젝트 */

    /**
     * @brief 활성화시, 리스트 갱신
     */
    void OnEnable()
    {
        this.ContentView.SetActive(false);

        this.Annots = new List<GameObject>();
    }

    /**
     * @brief 비활성화
     */
    void OnDisable()
    {
        this.ContentView.SetActive(false);
    }

    //PUBLIC
    /**
     * @brief 이미지 어노테이션 오브젝트 리스트 리턴
     * @returns 게임 오브젝트의 리스트
     */
    public List<GameObject> List()
    {
        return this.Annots;
    }

    /**
     * @brief 인덱스에 해당하는 게임 오브젝트 리턴
     * @param _idx 이미지 어노테이션 오브젝트 리스트의 인덱스, 0~
     * @returns 게임 오브젝트
     */
    public GameObject GetObject(int _idx)
    {
        return this.Annots[_idx];
    }

    /**
     * @brief 이미지 어노테이션 오브젝트 리스트 새 인덱스 리턴
     * @returns 인덱스 , 0~
     */
    public int NewIdx()
    {
        if (Annots.Count == 0) return 0;
        GameObject lastObj = Annots[Annots.Count - 1];
        return lastObj.GetComponent<ImageAnnotObjCtrl>().Idx + 1;
    }

    /**
     * @brief 이미지 어노테이션 오브젝트 리스트 갱신
     * @returns 인덱스 , 0~
     */
    public void BuildList()
    {
        if (this.Annots != null)
        {
            foreach (GameObject obj in this.Annots)
            {
                Destroy(obj);
            }
            this.Annots.Clear();
        }

        this.Annots = ProjCtrl.ImageAnnots(MainCtrl.CurScene);

        foreach (GameObject obj in this.Annots)
        {
            ImageAnnotObjCtrl annotObject = obj.GetComponent<ImageAnnotObjCtrl>();
            if (annotObject)
            {
                annotObject.HideAnnotContent = this.HideAnnotContent;
                annotObject.ShowAnnotContent = this.ShowAnnotContent;
                PanoramicViewCtrl.Set(obj, annotObject.Angle);
                PanoramicViewCtrl.Set(obj, annotObject.Angle);
            }
        }

        this.TargetObj = null;
    }

    /**
     * @brief 이미지 어노테이션 내용 가리기
     * @param _obj 이미지 어노테이션의 게임 오브젝트
     */
    public void HideAnnotContent(GameObject _obj = null)
    {
        Debug.Log("HideAnnotContent ");

        if (_obj != null && !GameObject.Equals(this.TargetObj, _obj)) return;
        this.TargetObj = null;

        this.ContentView.SetActive(false);
    }

    /**
     * @brief 이미지 어노테이션 내용 보이기
     * @param _obj 이미지 어노테이션의 게임 오브젝트
     * @param _name 어노테이션 이름
     * @param _texture 이미지 필드의 텍스쳐
     */
    public void ShowAnnotContent(GameObject _obj, string _name, Texture2D _texture)
    {
        Debug.Log("ShowAnnotContent " + _name);

        this.TargetObj = _obj;

        this.ContentView.GetComponent<ImageAnnotContentObjCtrl>().Set(_name, _texture);
        this.ContentView.SetActive(true);
    }

}
