﻿using System.Collections.Generic;
using AlertView.iOS;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Sample1 : MonoBehaviour
{
    public Text DebugText;
    public Button TestButton;
    public Button TestButton2;
    public Button NextSampleBtn;
    
    
    private void Awake()
    {
        TestButton.onClick.AddListener(ShowTestAlertView);
        TestButton2.onClick.AddListener(ShowTestAlertView2);
        
        NextSampleBtn.onClick.AddListener(() => {SceneManager.LoadScene("Sample2");});
    }

    private void ShowTestAlertView()
    {
        var alert = new AlertData(
            "This is title",
            "Here you can use your message!",
            AlertStyle.Alert,
            new List<ButtonData>()
            {
                new ButtonData("As", ButtonStyle.Default, CallbackHandlerAs),
                new ButtonData("Much", ButtonStyle.Default, CallbackHandlerMuch),
                new ButtonData("Buttons", ButtonStyle.Default, CallbackHandlerButtons),
                new ButtonData("As", ButtonStyle.Default, CallbackHandlerAsSecond),
                new ButtonData("You", ButtonStyle.Destructive, CallbackHandlerYou),
                new ButtonData("Wish", ButtonStyle.Cancel, CallbackHandlerWish),
            }
        );
        
        AlertViewManager.ShowAlertView(alert);
    }

    #region > Callbacks
    
    private void CallbackHandlerAs()
    {
        DebugText.text = "You Pressed \"As\"";
    }
    
    private void CallbackHandlerMuch()
    {
        DebugText.text = "You Pressed \"Much\"";
    }
    
    private void CallbackHandlerButtons()
    {
        DebugText.text = "You Pressed \"Buttons\"";
    }
    
    private void CallbackHandlerAsSecond()
    {
        DebugText.text = "You Pressed second \"As\"";
    }
    
    private void CallbackHandlerYou()
    {
        DebugText.text = "You Pressed \"You\"";
    }
    
    private void CallbackHandlerWish()
    {
        DebugText.text = "You Pressed \"Wish\"";
    }
    
    #endregion > Callbacks
    
    private void ShowTestAlertView2()
    {
        var alert = new AlertData(
            "This is title",
            "Here you can use your message!",
            AlertStyle.ActionSheet,
            new List<ButtonData>()
            {
                new ButtonData("Check", ButtonStyle.Default, CallbackHandlerCheck),
                new ButtonData("Out", ButtonStyle.Default, CallbackHandlerOut),
                new ButtonData("This", ButtonStyle.Destructive, CallbackHandlerThis),
                new ButtonData("Style", ButtonStyle.Cancel, CallbackHandlerStyle),
            }
        );
        
        AlertViewManager.ShowAlertView(alert);
    }

    #region > Callbacks Second
    
    private void CallbackHandlerCheck()
    {
        DebugText.text = "You Pressed \"Check\"";
    }
    
    private void CallbackHandlerOut()
    {
        DebugText.text = "You Pressed \"Out\"";
    }
    
    private void CallbackHandlerThis()
    {
        DebugText.text = "You Pressed \"This\"";
    }
    
    private void CallbackHandlerStyle()
    {
        DebugText.text = "You Pressed \"Style\"";
    }
    
    #endregion > Callbacks Second
}