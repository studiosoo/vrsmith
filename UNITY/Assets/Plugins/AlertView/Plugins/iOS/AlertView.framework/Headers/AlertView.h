//
//  AlertView.h
//  AlertView
//
//  Created by Frater Doctus on 4/24/18.
//  Copyright © 2018 Frater Doctus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

extern "C" {
    typedef void (*intCallbackFunc)(int number);
}

@interface AlertView : NSObject
{
    intCallbackFunc callback;
}

-(id)init:(intCallbackFunc) callbackFunc;

-(UIViewController*)getUIViewController;
-(UIAlertControllerStyle)getAlertControllerStyle: (int) styleNumber;
-(UIAlertActionStyle)getAlertActionStyle: (int) styleNumber;

-(void)showMultipleButtonsView:  (NSString*) title:
                                (NSString*) message:
                                (NSMutableArray*) buttons:
                                (int) alertActionStyle;

@end
