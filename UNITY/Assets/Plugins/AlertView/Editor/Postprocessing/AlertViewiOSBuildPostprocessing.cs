﻿using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using UnityEditor.iOS.Xcode.Extensions;

public class AlertViewiOSBuildPostprocessing
{
	[PostProcessBuild(100)]
	public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) 
	{
		#if UNITY_IOS
		//EmbedFrameworks
		var projPath = PBXProject.GetPBXProjectPath(pathToBuiltProject);
		var proj = new PBXProject();
		proj.ReadFromString(File.ReadAllText(projPath));
		var targetGuid = proj.TargetGuidByName("Unity-iPhone");
		const string defaultLocationInProj = "Plugins/iOS";
		const string coreFrameworkName = "AlertView.framework";
		var framework = Path.Combine(defaultLocationInProj, coreFrameworkName);
		var fileGuid = proj.AddFile(framework, "Frameworks/AlertView/" + framework, PBXSourceTree.Sdk);
		PBXProjectExtensions.AddFileToEmbedFrameworks(proj, targetGuid, fileGuid);
		proj.SetBuildProperty(targetGuid, "LD_RUNPATH_SEARCH_PATHS", "$(inherited) @executable_path/Frameworks");
		proj.SetBuildProperty(targetGuid, "ENABLE_BITCODE", "NO");
		proj.WriteToFile (projPath);
		//EmbedFrameworks end
		#endif
	}
}
