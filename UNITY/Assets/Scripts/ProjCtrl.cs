/**
* @file ProjCtrl.cs
* @date 2018/07/23
* @author KIMJISOO(kind.jamie@gmail.com)
* @brief 프로젝트 컨트롤 스크립트
*/
using System.IO;
using System.Xml;
using System.Collections.Generic;
using UnityEngine;

/**
* @class ProjCtrl
* @date 2018/07/23
* @brief 프로젝트 컨트롤
* 프로젝트 파일 압축/해제, 열기/닫기, Xml 컨텐츠 컨트롤
*/
public class ProjCtrl : MonoBehaviour {

    //DEFINE
    public const string CONTENT_FILE = "Content.xml";   /** 프로젝트 컨텐츠 파일 이름 */
    //DEFINE

    //SINGLETON
    static public ProjCtrl Instance = null;             /** 싱글턴 인스턴스 */

    public GameObject TextAnnotObj;                     /** 텍스트 어노테이션 오브젝트 */
    public GameObject ImageAnnotObj;                    /** 이미지 어노테이션 오브젝트 */
    public GameObject LinkAnnotObj;                     /** 이미지 어노테이션 오브젝트 */

    bool Edited = false;                                /** 프로젝트 편집 여부 */
    XmlDocument XmlDoc = null;                          /** XML 도큐먼트 인스턴스 */

    /**
     * @brief 앱 구동시, 싱글턴 인스턴스 설정
     */
    void Awake()
    {
        if (Instance == null) Instance = this;
    }

    //PATH
    /**
     * @brief TEMP_DIR 초기화 
     */
    public void ClearTEMP() 
    {
        MainCtrl.TEMP_DIR = Path.Combine(Application.persistentDataPath, "TEMP");
        if (Directory.Exists(MainCtrl.TEMP_DIR))
        {
            Directory.Delete(MainCtrl.TEMP_DIR, true);
        }
        Directory.CreateDirectory(MainCtrl.TEMP_DIR);     
    }

    //PROJECT
    /**
     * @brief 기본 내용이 입력된 Content.xml 파일 생성 
     * @param _path 컨텐츠 파일 경로
     * @param _projName 프로젝트 이름
     */
    public void NewContentXML(string _path, string _projName)
    {
        var file = File.CreateText(_path);
        file.WriteLine("<?xml version = \"1.0\" encoding = \"UTF-8\" ?>");
        file.WriteLine("<virtualsmith version = \"1.0\">");
        file.WriteLine("\t<project>");
        file.WriteLine("\t\t<name>" + _projName + "</name>");
        file.WriteLine("\t\t<scenes>");
        file.WriteLine("\t\t\t<scene>");
        file.WriteLine("\t\t\t\t<name>" + _projName + "</name>");
        file.WriteLine("\t\t\t\t<image></image>");
        file.WriteLine("\t\t\t\t<location><latitude></latitude><longitude></longitude></location>");
        file.WriteLine("\t\t\t\t<annots></annots>");
        file.WriteLine("\t\t\t</scene>");
        file.WriteLine("\t\t</scenes>");
        file.WriteLine("\t</project>");
        file.WriteLine("</virtualsmith>");
        file.Close();
    }

    /**
     * @brief 프로젝트 생성 
     * @param _projName 프로젝트 이름
     * @param _imgScenePath Scene 이미지 경로, 파일 이름 확장자 포함
     */
    static public void CreateProject(string _projName, string _imgScenePath)
    {
        string xml = Path.Combine(MainCtrl.TEMP_DIR, CONTENT_FILE);

        ProjCtrl.Instance.ClearTEMP();
        ProjCtrl.Instance.NewContentXML(xml, _projName);

        string image = _projName + Path.GetExtension(_imgScenePath);
        string path = Path.Combine(MainCtrl.TEMP_DIR, image);
        File.Copy(_imgScenePath, path);

        ProjCtrl.Instance.LoadXML(xml);
        ProjCtrl.Instance.Edited = true;
        MainCtrl.CurScene = 0;

        ProjCtrl.ProjectName(_projName);
        ProjCtrl.SceneName(MainCtrl.CurScene, _projName);
        ProjCtrl.SceneImage(MainCtrl.CurScene, image);
        ProjCtrl.SceneLocation(MainCtrl.CurScene, ImageAnnotObjCtrl.GPSLocation(image));
    }

    /**
     * @brief 프로젝트 열기
     * @param _projPath 프로젝트 파일 경로
     */
    static public void OpenProject(string _projPath)
    {
        ProjCtrl.Instance.ClearTEMP();

        ProjCtrl.Instance.Decompress(_projPath, MainCtrl.TEMP_DIR);

        ProjCtrl.Instance.LoadXML(Path.Combine(MainCtrl.TEMP_DIR, CONTENT_FILE));
        Debug.Log("Xml Content Version " + ProjCtrl.XmlVersion());
        ProjCtrl.Instance.Edited = false;
        MainCtrl.CurScene = 0;
    }

    /**
     * @brief 프로젝트 저장
     * PROJ_DIR에 [프로젝트이름].zip 으로 저장
     * 동일한 이름의 프로젝트가 있으면 기존 프로젝트를 .bak로 백업
     */
    static public void SaveProject()
    {
        if (ProjCtrl.Instance.Edited == false) return;

        if (ProjCtrl.Instance.XmlDoc == null) return;

        Debug.Log("SaveProject " + ProjCtrl.ProjectName());

        ProjCtrl.Instance.SaveXML(Path.Combine(MainCtrl.TEMP_DIR, CONTENT_FILE));
        string zipFile = ProjCtrl.ProjectName() + ".zip";
        ProjCtrl.Instance.Compress(MainCtrl.TEMP_DIR, Path.Combine(MainCtrl.TEMP_DIR, zipFile));
        string bakFile = ProjCtrl.ProjectName() + ".bak";
        if (File.Exists(Path.Combine(MainCtrl.PROJ_DIR, zipFile)))
        {
            File.Replace(Path.Combine(MainCtrl.TEMP_DIR, zipFile), Path.Combine(MainCtrl.PROJ_DIR, zipFile), Path.Combine(MainCtrl.PROJ_DIR, bakFile));    
        }
        else 
        {
            File.Copy(Path.Combine(MainCtrl.TEMP_DIR, zipFile), Path.Combine(MainCtrl.PROJ_DIR, zipFile));
        }

        ProjCtrl.CloseProject();
    }

    /**
     * @brief 프로젝트 닫기
     */
    static public void CloseProject()
    {
        ProjCtrl.Instance.Edited = false;

        ProjCtrl.Instance.XmlDoc = null;
    }

    //XML
    /**
     * @brief 컨텐츠 파일 읽기
     * @param _xmlFile Content.xml 파일 경로
     */
    void LoadXML(string _xmlFile)
    {
        ProjCtrl.Instance.XmlDoc = new XmlDocument();
        ProjCtrl.Instance.XmlDoc.LoadXml(File.ReadAllText(_xmlFile));
    }

    /**
     * @brief Xml 도큐먼트 저장
     * @param _xmlFile Content.xml 파일 경로
     */
    void SaveXML(string _xmlFile)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return;
        if (ProjCtrl.Instance.Edited == false) return;

        ProjCtrl.Instance.XmlDoc.Save(_xmlFile);
    }

    /**
     * @brief 콘텐츠 버전 리턴
     * @return 버전 정보 문자열
     */
    static public string XmlVersion()
    {
        if (ProjCtrl.Instance.XmlDoc == null)
            return "null";

        XmlNode node = ProjCtrl.Instance.XmlDoc.SelectSingleNode("virtualsmith").
                               Attributes.GetNamedItem("version");
        if (node != null)
        {
            return node.Value;
        }
            
        return "0";
    }

    /**
     * @brief 프로젝트 이름 리턴
     * @return 이름 문자열
     */
    static public string ProjectName()
    {
        if (ProjCtrl.Instance.XmlDoc == null) 
            return "xmlDoc == null";

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/name");
        foreach (XmlNode node in nodes) 
        {
            return node.InnerText;
        }

        return "VS-PROJECT";
    }

    /**
     * @brief Scene 개수 리턴
     * @return 개수
     */
    static public int SceneCount()
    {
        if (ProjCtrl.Instance.XmlDoc == null) return 0;

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");

        return nodes.Count;
    }

    /**
     * @brief Scene 이름에 대항하는 Idx 리턴
     * @return Scene 인덱스, 0~
     */
    static public int SceneIdx(string _sceneName)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return ProjCtrl.SceneCount();

        for (int i = 0; i < ProjCtrl.SceneCount(); i++)
        {
            if (ProjCtrl.SceneName(i) == _sceneName) return i;
        }
        return ProjCtrl.SceneCount();
    }

    /**
     * @brief Scene 이름 리턴
     * @param _sceneIdx Scene 인덱스, 0~
     * @return 이름 문자열
     */
    static public string SceneName(int _sceneIdx)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return "";

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode node = nodes.Item(_sceneIdx).SelectSingleNode("name");

        return node.InnerText;
    }

    /**
     * @brief Scene 이미지 파일 이름 리턴
     * @param _sceneIdx Scene 인덱스, 0~
     * @return 파일 이름 문자열
     */
    static public string SceneImage(int _sceneIdx)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return "";

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode node = nodes.Item(_sceneIdx).SelectSingleNode("image");

        return node.InnerText;
    }

    /**
     * @brief Scene 위치 정보 리턴
     * @param _sceneIdx Scene 인덱스, 0~
     * @return 위도, 경도 정보가 설정된 Vector2, 없으면 zero
     */
    static public Vector2 SceneLocation(int _sceneIdx)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return Vector2.zero;

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode node = nodes.Item(_sceneIdx).SelectSingleNode("location");
        if (node != null)
        {
            XmlNode latitude = node.SelectSingleNode("latitude");
            XmlNode longitude = node.SelectSingleNode("longitude");
            return new Vector2(ToFloat(latitude.InnerText), ToFloat(longitude.InnerText));
        }

        return Vector2.zero;
    }

    /**
     * @brief 텍스트 어노테이션 오브젝트 리턴
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _idx 텍스트 어노테이션 인텍스, 0~
     * @return 어노테이션 게임 오브젝트
     */
    static public GameObject TextAnnot(int _sceneIdx, int _idx)
    {
        foreach (GameObject obj in ProjCtrl.Instance._TextAnnots(_sceneIdx))
        {
            if (obj.GetComponent<TextAnnotObjCtrl>().Idx == _idx) return obj;
        }

        return null;
    }

    /**
     * @brief 텍스트 어노테이션 리스트 리턴
     * @param _sceneIdx Scene 인덱스, 0
     * @return 게임 오브젝트의 리스트
     */
    static public List<GameObject> TextAnnots(int _sceneIdx)
    {
        return ProjCtrl.Instance._TextAnnots(_sceneIdx);
    }

    /**
     * @brief 텍스트 어노테이션 리스트 리턴
     * @param _sceneIdx Scene 인덱스, 0
     * @return 게임 오브젝트의 리스트
     */
    List<GameObject> _TextAnnots(int _sceneIdx)
    {
        List<GameObject> objects = new List<GameObject>();

        if (this.XmlDoc == null) return objects;

        XmlNodeList nodes = this.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode annots = nodes.Item(_sceneIdx).SelectSingleNode("annots");
        foreach (XmlNode node in annots.SelectNodes("textannot")) 
        {
            string annotname = node.SelectSingleNode("name").SelectSingleNode("font").InnerText;
            Vector3 angle = ProjCtrl.ToVector3(node.SelectSingleNode("angle").InnerText);
            string createdtime = node.SelectSingleNode("createdtime").InnerText;
            string modifiedtime = node.SelectSingleNode("modifiedtime").InnerText;
            //content
            string contentPos = node.SelectSingleNode("content/pos").InnerText;
            string contentShape = node.SelectSingleNode("content/shape").InnerText;
            string contentDesc = node.SelectSingleNode("content/desc").SelectSingleNode("font").InnerText;

            var obj = _NewTextAnnot();
            if (obj) 
            {
                obj.GetComponent<TextAnnotObjCtrl>().Set(objects.Count, 
                                                         angle, 
                                                         contentPos, 
                                                         annotname, 
                                                         createdtime, 
                                                         modifiedtime, 
                                                         contentDesc);
                objects.Add(obj);
            }
        }

        return objects;
    }

    /**
     * @brief 이미지 어노테이션 리스트 리턴
     * @param _sceneIdx Scene 인덱스, 0
     * @return 게임 오브젝트의 리스트
     */
    static public List<GameObject> ImageAnnots(int _sceneIdx)
    {
        return ProjCtrl.Instance._ImageAnnots(_sceneIdx);
    }

    /**
     * @brief 이미지 어노테이션 리스트 리턴
     * @param _sceneIdx Scene 인덱스, 0
     * @return 게임 오브젝트의 리스트
     */
    List<GameObject> _ImageAnnots(int _sceneIdx)
    {
        List<GameObject> objects = new List<GameObject>();

        if (this.XmlDoc == null) return objects;

        XmlNodeList nodes = this.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode annots = nodes.Item(_sceneIdx).SelectSingleNode("annots");
        foreach (XmlNode node in annots.SelectNodes("imageannot"))
        {
            string annotname = node.SelectSingleNode("name").SelectSingleNode("font").InnerText;
            Vector3 angle = ProjCtrl.ToVector3(node.SelectSingleNode("angle").InnerText);
            string createdtime = node.SelectSingleNode("createdtime").InnerText;
            string modifiedtime = node.SelectSingleNode("modifiedtime").InnerText;
            //content
            string contentImage = node.SelectSingleNode("content/image").InnerText;
            string path = Path.Combine(MainCtrl.TEMP_DIR, contentImage);
            string rotation = node.SelectSingleNode("content/rotation").InnerText;

            var obj = _NewImageAnnot();
            if (obj)
            {
                obj.GetComponent<ImageAnnotObjCtrl>().Set(objects.Count, 
                                                          annotname, 
                                                          angle, 
                                                          createdtime, 
                                                          modifiedtime, 
                                                          path, 
                                                          int.Parse(rotation));
                objects.Add(obj);
            }
        }

        return objects;
    }


    /**
     * @brief 링크 어노테이션 리스트 리턴
     * @param _sceneIdx Scene 인덱스, 0
     * @return 게임 오브젝트의 리스트
     */
    static public List<GameObject> LinkAnnots(int _sceneIdx)
    {
        return ProjCtrl.Instance._LinkAnnots(_sceneIdx);
    }

    /**
     * @brief 링크 어노테이션 리스트 리턴
     * @param _sceneIdx Scene 인덱스, 0
     * @return 게임 오브젝트의 리스트
     */
    List<GameObject> _LinkAnnots(int _sceneIdx)
    {
        List<GameObject> objects = new List<GameObject>();

        if (this.XmlDoc == null) return objects;

        XmlNodeList nodes = this.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode annots = nodes.Item(_sceneIdx).SelectSingleNode("annots");
        foreach (XmlNode node in annots.SelectNodes("linkannot"))
        {
            string annotname = node.SelectSingleNode("name").SelectSingleNode("font").InnerText;
            Vector3 angle = ProjCtrl.ToVector3(node.SelectSingleNode("angle").InnerText);
            string createdtime = node.SelectSingleNode("createdtime").InnerText;
            string modifiedtime = node.SelectSingleNode("modifiedtime").InnerText;
            //content
            string scene = node.SelectSingleNode("content/scene").InnerText;

            var obj = _NewLinkAnnot();
            if (obj)
            {
                obj.GetComponent<LinkAnnotObjCtrl>().Set(objects.Count,
                                                         annotname,
                                                         angle,
                                                         createdtime,
                                                         modifiedtime,
                                                         scene);
                objects.Add(obj);
            }
        }

        return objects;
    }

    /**
     * @brief 프로젝트 이름 설정
     * @param _name 프로젝트 이름
     */
    static public void ProjectName(string _name)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return;

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/name");
        foreach (XmlNode node in nodes)
        {
            node.InnerText = _name;
            ProjCtrl.Instance.Edited = true;
        }
    }

    /**
     * @brief 새 Scene XmlNode 리턴
     * @return XmlNode
     */
    static public XmlNode NewSceneNode()
    {
        XmlNode node = ProjCtrl.Instance.XmlDoc.CreateElement("scene");

        XmlNode name = ProjCtrl.Instance.XmlDoc.CreateElement("name");
        name.InnerText = "scene name";
        node.AppendChild(name);

        XmlNode image = ProjCtrl.Instance.XmlDoc.CreateElement("image");
        image.InnerText = "scene image";
        node.AppendChild(image);

        XmlNode location = ProjCtrl.Instance.XmlDoc.CreateElement("location");
        location.InnerText = "";
        {
            XmlNode latitude = ProjCtrl.Instance.XmlDoc.CreateElement("latitude");
            latitude.InnerText = "";
            location.AppendChild(latitude);

            XmlNode longitude = ProjCtrl.Instance.XmlDoc.CreateElement("longitude");
            longitude.InnerText = "";
            location.AppendChild(longitude);
        }
        node.AppendChild(location);

        XmlNode annots = ProjCtrl.Instance.XmlDoc.CreateElement("annots");
        node.AppendChild(annots);

        return node;
    }

    /**
     * @brief 새 Scene 노드 추가
     * @return 추가된 Scene의 인덱스, 0~
     * @todo on error return value
     */
    static public int AppendScene() 
    {
        if (ProjCtrl.Instance.XmlDoc == null) return -1;

        XmlNode scenes = ProjCtrl.Instance.XmlDoc.SelectSingleNode("virtualsmith/project/scenes");
        XmlNode scene = ProjCtrl.NewSceneNode();
        scenes.AppendChild(scene);
        ProjCtrl.Instance.Edited = true;

        return (ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene").Count - 1);
    }

    /**
     * @brief Scene 이름 설정
     * @param _sceneIdx Scene 인덱스, 0
     * @param _name Scene 이름
     */
    static public void SceneName(int _sceneIdx, string _name)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return;

        XmlNodeList scenes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode scene = scenes.Item(_sceneIdx);
        if (scene != null) 
        {
            XmlNode node = scene.SelectSingleNode("name");    
            node.InnerText = _name;
            ProjCtrl.Instance.Edited = true;
        }
    }

    /**
     * @brief Scene 이미지 설정
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _img Scene 이미지 파일 이름
     */
    static public void SceneImage(int _sceneIdx, string _img)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return;

        XmlNodeList scenes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode scene = scenes.Item(_sceneIdx);
        if (scene != null)
        {
            XmlNode node = scene.SelectSingleNode("image");
            node.InnerText = _img;
            ProjCtrl.Instance.Edited = true;
        }
    }

    /**
     * @brief Scene 위치 설정
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _location 위치 정보
     */
    static public void SceneLocation(int _sceneIdx, Vector2 _location)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return;

        XmlNodeList scenes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode scene = scenes.Item(_sceneIdx);
        if (scene != null)
        {
            XmlNode node = scene.SelectSingleNode("location");
            if (node != null)
            {
                XmlNode latitude = node.SelectSingleNode("latitude");
                latitude.InnerText = _location.x.ToString();
                XmlNode longitude = node.SelectSingleNode("longitude");
                longitude.InnerText = _location.y.ToString();   
                ProjCtrl.Instance.Edited = true;
            }    
        }
    }

    /**
     * @brief 텍스트 어노테이션 XmlNode 리턴
     * @param _obj 텍스트 어노테이션의 게임 오브젝트
     * @return XmlNode
     */
    static public XmlNode TextAnnotNode(GameObject _obj)
    {
        XmlDocument xmlDocument = ProjCtrl.Instance.XmlDoc;

        XmlNode node = xmlDocument.CreateElement("textannot");
     
        TextAnnotObjCtrl obj = _obj.GetComponent<TextAnnotObjCtrl>();
        if (obj)
        {
            XmlNode name = xmlDocument.CreateElement("name");
            XmlNode namefont = xmlDocument.CreateElement("font");
            namefont.InnerText = obj.Name;
            name.AppendChild(namefont);
            node.AppendChild(name);
            //Debug.Log("TextAnnotNode name.InnerText " + name.InnerText);

            XmlNode angle = xmlDocument.CreateElement("angle");
            angle.InnerText = ProjCtrl.ToAngleString(obj.Angle);
            node.AppendChild(angle);
            //Debug.Log("TextAnnotNode angle.InnerText " + angle.InnerText);

            XmlNode createdtime = xmlDocument.CreateElement("createdtime");
            createdtime.InnerText = obj.CreatedTime;
            node.AppendChild(createdtime);
            //Debug.Log("TextAnnotNode createdtime.InnerText " + createdtime.InnerText);

            XmlNode modifiedtime = xmlDocument.CreateElement("modifiedtime");
            modifiedtime.InnerText = obj.ModifiedTime;
            node.AppendChild(modifiedtime);
            //Debug.Log("TextAnnotNode modifiedtime.InnerText " + modifiedtime.InnerText);

            XmlNode content = xmlDocument.CreateElement("content");
            {
                XmlNode pos = xmlDocument.CreateElement("pos");
                pos.InnerText = obj.Pos;
                content.AppendChild(pos);
                //Debug.Log("TextAnnotNode pos.InnerText " + pos.InnerText);

                XmlNode shape = xmlDocument.CreateElement("shape");
                shape.InnerText = "default";//obj.Shape;
                content.AppendChild(shape);
                //Debug.Log("TextAnnotNode shape.InnerText " + shape.InnerText);

                XmlNode desc = xmlDocument.CreateElement("desc");
                XmlNode descfont = xmlDocument.CreateElement("font");
                descfont.InnerText = obj.Desc;
                desc.AppendChild(descfont);
                content.AppendChild(desc);
                //Debug.Log("TextAnnotNode desc.InnerText " + desc.InnerText);
            }
            node.AppendChild(content);

        }
        return node;
    }

    /**
     * @brief 텍스트 어노테이션 설정
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _obj 텍스트 어노테이션의 게임 오브젝트
     */
    static public void TextAnnot(int _sceneIdx, GameObject _obj)
    {
        //Debug.Log("TextAnnot " + _sceneIdx.ToString());

        if (ProjCtrl.Instance.XmlDoc == null) return;

        TextAnnotObjCtrl obj = _obj.GetComponent<TextAnnotObjCtrl>();
            
        //Debug.Log("TextAnnot obj.Idx " + obj.Idx.ToString());

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode annots = nodes.Item(_sceneIdx).SelectSingleNode("annots");
        XmlNode annot = annots.SelectNodes("textannot").Item(obj.Idx);
        if (annot == null) {
            annots.AppendChild(ProjCtrl.TextAnnotNode(_obj));
        } else {
            XmlNode node = annot.SelectSingleNode("name/font");
            node.InnerText = obj.Name;
            //Debug.Log("TextAnnot name.InnerText " + node.InnerText);

            node = annot.SelectSingleNode("angle");
            node.InnerText = ProjCtrl.ToAngleString(obj.Angle);
            //Debug.Log("TextAnnot angle.InnerText " + node.InnerText);

            node = annot.SelectSingleNode("createdtime");
            node.InnerText = obj.CreatedTime;
            //Debug.Log("TextAnnot createdtime.InnerText " + node.InnerText);

            node = annot.SelectSingleNode("modifiedtime");
            node.InnerText = obj.ModifiedTime;
            //Debug.Log("TextAnnot modifiedtime.InnerText " + node.InnerText);

            {
                node = annot.SelectSingleNode("content/pos");
                node.InnerText = obj.Pos;
                //Debug.Log("TextAnnot pos.InnerText " + node.InnerText);

                node = annot.SelectSingleNode("content/shape");
                node.InnerText = "default";//obj.Shape;
                //Debug.Log("TextAnnot shape.InnerText " + node.InnerText);

                node = annot.SelectSingleNode("content/desc/font");
                node.InnerText = obj.Desc;
                //Debug.Log("TextAnnot desc.InnerText " + node.InnerText);
            }
        }

        ProjCtrl.Instance.Edited = true;
    }

    /**
     * @brief 텍스트 어노테이션 설정
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _objs 텍스트 어노테이션의 게임 오브젝트 리스트
     */
    static public void TextAnnots(int _sceneIdx, List<GameObject> _objs)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return;

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode annots = nodes.Item(_sceneIdx).SelectSingleNode("annots");
        foreach (XmlNode textannot in annots.SelectNodes("textannot"))
        {
            annots.RemoveChild(textannot);
        }
        foreach (GameObject obj in _objs)
        {
            annots.AppendChild(ProjCtrl.TextAnnotNode(obj));
        }
    }

    /**
     * @brief 새 텍스트 어노테이션 오브젝트
     * @returns 텍스트 어노테이션의 게임 오브젝트
     */
    static public GameObject NewTextAnnot()
    {
        return ProjCtrl.Instance._NewTextAnnot();
    }

    /**
     * @brief 새 텍스트 어노테이션 오브젝트
     * @returns 텍스트 어노테이션의 게임 오브젝트
     */
    GameObject _NewTextAnnot()
    {
        return Instantiate(this.TextAnnotObj) as GameObject;
    }

    /**
     * @brief 텍스트 어노테이션 삭제
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _idx 텍스트 어노테이션 인덱스, 0~
     */
    static public void DelTextAnnot(int _sceneIdx, int _idx)
    {
        ProjCtrl.Instance._DelTextAnnot(_sceneIdx, _idx);    
    }

    /**
     * @brief 텍스트 어노테이션 삭제
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _idx 텍스트 어노테이션 인덱스, 0~
     */
    void _DelTextAnnot(int _sceneIdx, int _idx)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return;

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode annots = nodes.Item(_sceneIdx).SelectSingleNode("annots");
        XmlNode annot = annots.SelectNodes("textannot").Item(_idx);
        if (annot != null) {
            XmlNode parentNode = annot.ParentNode;
            parentNode.RemoveChild(annot);   
        }

        ProjCtrl.Instance.Edited = true;
    }

    /**
     * @brief 이미지 어노테이션 XmlNode 리턴
     * @param _obj 이미지 어노테이션의 게임 오브젝트
     * @return XmlNode
     */
    static public XmlNode ImageAnnotNode(GameObject _obj)
    {
        XmlDocument xmlDocument = ProjCtrl.Instance.XmlDoc;

        XmlNode node = xmlDocument.CreateElement("imageannot");

        ImageAnnotObjCtrl obj = _obj.GetComponent<ImageAnnotObjCtrl>();
        if (obj)
        {
            XmlNode name = xmlDocument.CreateElement("name");
            XmlNode namefont = xmlDocument.CreateElement("font");
            namefont.InnerText = obj.Name;
            name.AppendChild(namefont);
            node.AppendChild(name);

            XmlNode angle = xmlDocument.CreateElement("angle");
            angle.InnerText = ProjCtrl.ToAngleString(obj.Angle);
            node.AppendChild(angle);

            XmlNode createdtime = xmlDocument.CreateElement("createdtime");
            createdtime.InnerText = obj.CreatedTime;
            node.AppendChild(createdtime);

            XmlNode modifiedtime = xmlDocument.CreateElement("modifiedtime");
            modifiedtime.InnerText = obj.ModifiedTime;
            node.AppendChild(modifiedtime);

            XmlNode content = xmlDocument.CreateElement("content");
            {
                XmlNode image = xmlDocument.CreateElement("image");
                image.InnerText = Path.GetFileName(obj.ImagePath);
                content.AppendChild(image);

                XmlNode rotation = xmlDocument.CreateElement("rotation");
                rotation.InnerText = obj.ImageRotation.ToString();
                content.AppendChild(rotation);
            }
            node.AppendChild(content);

        }
        return node;
    }

    /**
     * @brief 이미지 어노테이션 설정
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _obj 이미지 어노테이션의 게임 오브젝트
     */
    static public void ImageAnnot(int _sceneIdx, GameObject _obj)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return;

        ImageAnnotObjCtrl obj = _obj.GetComponent<ImageAnnotObjCtrl>();

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode annots = nodes.Item(_sceneIdx).SelectSingleNode("annots");
        XmlNode annot = annots.SelectNodes("imageannot").Item(obj.Idx);
        if (annot == null)
        {
            annots.AppendChild(ProjCtrl.ImageAnnotNode(_obj));
        }
        else
        {
            XmlNode node = annot.SelectSingleNode("name/font");
            node.InnerText = obj.Name;

            node = annot.SelectSingleNode("angle");
            node.InnerText = ProjCtrl.ToAngleString(obj.Angle);

            node = annot.SelectSingleNode("createdtime");
            node.InnerText = obj.CreatedTime;

            node = annot.SelectSingleNode("modifiedtime");
            node.InnerText = obj.ModifiedTime;

            {
                node = annot.SelectSingleNode("content/image");
                node.InnerText = Path.GetFileName(obj.ImagePath);

                node = annot.SelectSingleNode("content/rotation");
                node.InnerText = obj.ImageRotation.ToString();
            }
        }

        ProjCtrl.Instance.Edited = true;
    }

    /**
     * @brief 이미지 어노테이션 설정
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _objs 이미지 어노테이션의 게임 오브젝트 리스트
     */
    static public void ImageAnnots(int _sceneIdx, List<GameObject> _objs)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return;

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode annots = nodes.Item(_sceneIdx).SelectSingleNode("annots");
        foreach (XmlNode textannot in annots.SelectNodes("imageannot"))
        {
            annots.RemoveChild(textannot);
        }
        foreach (GameObject obj in _objs)
        {
            annots.AppendChild(ProjCtrl.ImageAnnotNode(obj));
        }
    }

    /**
     * @brief 새 이미지 어노테이션 오브젝트
     * @returns 이미지 어노테이션의 게임 오브젝트
     */
    static public GameObject NewImageAnnot()
    {
        return ProjCtrl.Instance._NewImageAnnot();
    }

    /**
     * @brief 새 이미지 어노테이션 오브젝트
     * @returns 이미지 어노테이션의 게임 오브젝트
     */
    GameObject _NewImageAnnot()
    {
        return Instantiate(this.ImageAnnotObj) as GameObject;
    }

    /**
     * @brief 이미지 어노테이션 삭제
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _idx 이미지 어노테이션 인덱스, 0~
     */
    static public void DelImageAnnot(int _sceneIdx, int _idx)
    {
        ProjCtrl.Instance._DelImageAnnot(_sceneIdx, _idx);
    }

    /**
     * @brief 이미지 어노테이션 삭제
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _idx 이미지 어노테이션 인덱스, 0~
     */
    void _DelImageAnnot(int _sceneIdx, int _idx)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return;

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode annots = nodes.Item(_sceneIdx).SelectSingleNode("annots");
        XmlNode annot = annots.SelectNodes("imageannot").Item(_idx);
        if (annot != null)
        {
            XmlNode parentNode = annot.ParentNode;
            parentNode.RemoveChild(annot);
        }

        ProjCtrl.Instance.Edited = true;
    }

    //LINK
    /**
     * @brief 이미지 어노테이션 XmlNode 리턴
     * @param _obj 이미지 어노테이션의 게임 오브젝트
     * @return XmlNode
     */
    static public XmlNode LinkAnnotNode(GameObject _obj)
    {
        XmlDocument xmlDocument = ProjCtrl.Instance.XmlDoc;

        XmlNode node = xmlDocument.CreateElement("linkannot");

        LinkAnnotObjCtrl obj = _obj.GetComponent<LinkAnnotObjCtrl>();
        if (obj)
        {
            XmlNode name = xmlDocument.CreateElement("name");
            XmlNode namefont = xmlDocument.CreateElement("font");
            namefont.InnerText = obj.Name;
            name.AppendChild(namefont);
            node.AppendChild(name);

            XmlNode angle = xmlDocument.CreateElement("angle");
            angle.InnerText = ProjCtrl.ToAngleString(obj.Angle);
            node.AppendChild(angle);

            XmlNode createdtime = xmlDocument.CreateElement("createdtime");
            createdtime.InnerText = obj.CreatedTime;
            node.AppendChild(createdtime);

            XmlNode modifiedtime = xmlDocument.CreateElement("modifiedtime");
            modifiedtime.InnerText = obj.ModifiedTime;
            node.AppendChild(modifiedtime);

            XmlNode content = xmlDocument.CreateElement("content");
            {
                XmlNode scene = xmlDocument.CreateElement("scene");
                scene.InnerText = obj.SceneName;
                content.AppendChild(scene);
            }
            node.AppendChild(content);

        }
        return node;
    }

    /**
     * @brief 이미지 어노테이션 설정
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _obj 이미지 어노테이션의 게임 오브젝트
     */
    static public void LinkAnnot(int _sceneIdx, GameObject _obj)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return;

        LinkAnnotObjCtrl obj = _obj.GetComponent<LinkAnnotObjCtrl>();

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode annots = nodes.Item(_sceneIdx).SelectSingleNode("annots");
        XmlNode annot = annots.SelectNodes("linkannot").Item(obj.Idx);
        if (annot == null)
        {
            annots.AppendChild(ProjCtrl.LinkAnnotNode(_obj));
        }
        else
        {
            XmlNode node = annot.SelectSingleNode("name/font");
            node.InnerText = obj.Name;

            node = annot.SelectSingleNode("angle");
            node.InnerText = ProjCtrl.ToAngleString(obj.Angle);

            node = annot.SelectSingleNode("createdtime");
            node.InnerText = obj.CreatedTime;

            node = annot.SelectSingleNode("modifiedtime");
            node.InnerText = obj.ModifiedTime;

            {
                node = annot.SelectSingleNode("content/scene");
                node.InnerText = obj.SceneName;
            }
        }

        ProjCtrl.Instance.Edited = true;
    }

    /**
     * @brief 이미지 어노테이션 설정
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _objs 이미지 어노테이션의 게임 오브젝트 리스트
     */
    static public void LinkAnnots(int _sceneIdx, List<GameObject> _objs)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return;

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode annots = nodes.Item(_sceneIdx).SelectSingleNode("annots");
        foreach (XmlNode annot in annots.SelectNodes("linkannot"))
        {
            annots.RemoveChild(annot);
        }
        foreach (GameObject obj in _objs)
        {
            annots.AppendChild(ProjCtrl.LinkAnnotNode(obj));
        }
    }

    /**
     * @brief 새 이미지 어노테이션 오브젝트
     * @returns 이미지 어노테이션의 게임 오브젝트
     */
    static public GameObject NewLinkAnnot()
    {
        return ProjCtrl.Instance._NewLinkAnnot();
    }

    /**
     * @brief 새 이미지 어노테이션 오브젝트
     * @returns 이미지 어노테이션의 게임 오브젝트
     */
    GameObject _NewLinkAnnot()
    {
        return Instantiate(this.LinkAnnotObj) as GameObject;
    }

    /**
     * @brief 이미지 어노테이션 삭제
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _idx 이미지 어노테이션 인덱스, 0~
     */
    static public void DelLinkAnnot(int _sceneIdx, int _idx)
    {
        ProjCtrl.Instance._DelLinkAnnot(_sceneIdx, _idx);
    }

    /**
     * @brief 이미지 어노테이션 삭제
     * @param _sceneIdx Scene 인덱스, 0~
     * @param _idx 이미지 어노테이션 인덱스, 0~
     */
    void _DelLinkAnnot(int _sceneIdx, int _idx)
    {
        if (ProjCtrl.Instance.XmlDoc == null) return;

        XmlNodeList nodes = ProjCtrl.Instance.XmlDoc.SelectNodes("virtualsmith/project/scenes/scene");
        XmlNode annots = nodes.Item(_sceneIdx).SelectSingleNode("annots");
        XmlNode annot = annots.SelectNodes("linkannot").Item(_idx);
        if (annot != null)
        {
            XmlNode parentNode = annot.ParentNode;
            parentNode.RemoveChild(annot);
        }

        ProjCtrl.Instance.Edited = true;
    }

    //ZIP
    /**
     * @brief 압축 풀기
     * @param _zipFile zip 파일 경로
     * @param _outDir 압축을 푼 파일의 경로, TEMP_DIR
     */
    void Decompress(string _zipFile, string _outDir)
    {
        int[] progress = new int[1];
        int[] progress2 = new int[1];
        int res = lzip.decompress_File(_zipFile, _outDir, progress, null, progress2);
        if (res == 1) Debug.Log("Decompress Success"); 
        else Debug.Log("Decompress Fail: " + res.ToString());
    }

    /**
     * @brief 압축
     * @param _srcDir 압축할 파일이 있는 폴더 경로, TEMP_DIR
     * @param _zipFile 압축 파일의 경로
     */
    void Compress(string _srcDir, string _zipFile)
    {
        lzip.compressDir(_srcDir, 9, _zipFile, false, null);
        Debug.Log("Compress " + lzip.cProgress.ToString() + " files");
    }

    //UTIL
    /**
     * @brief 현재 시간 문자열 리턴
     * @returns 현재 시간 문자열
     * "yyyy-MM-ddTH:mm:sszzz"
     */
    static public string Now()
    {
        return System.DateTime.Now.ToString("yyyy-MM-ddTH:mm:sszzz");
    }

    /**
     * @brief 현재 시간 문자열 리턴
     * @returns 현재 시간 문자열
     * "yyyyMMddHHmmss"
     */
    static public string DateTime()
    {
        return System.DateTime.Now.ToString("yyyyMMddHHmmss");
    }

    /**
     * @brief Int형 변환
     * @param _src 문자열
     * @param _default 기본값
     * @returns int value
     */
    static public int ToInt(string _src, int _default = 0)
    {
        int value = _default;
        int.TryParse(_src, out value);
        return value;
    }

    /**
     * @brief Float형 변환
     * @param _src 문자열
     * @param _default 기본값
     * @returns float value
     */
    static public float ToFloat(string _src, float _default = 0.0f)
    {
        float value = _default;
        float.TryParse(_src.Replace('f', ' ').Trim(), out value);
        return value;
    }

    /**
     * @brief 위치 문자열을 Vector3로 변환
     * @param _src 문자열
     * @returns Vector3 value
     */
    static public Vector3 ToVector3(string _src)
    {
        string[] list = _src.Replace("(", "").Replace(")", "").Split(' ');
        if (list.Length != 3) return Vector3.zero;

        return ProjCtrl.ToAngleRange(
            ProjCtrl.ToFloat(list[0], 0.0f),
            ProjCtrl.ToFloat(list[1], 0.0f), 0.0f);
    }

    /**
     * @brief Vector3를 위치 문자열로 변환
     * @param _src 위치
     * @returns 위치 문자열
     */
    static public string ToAngleString(Vector3 _src)
    {
        return _src.ToString().Replace("(", "").Replace(")", "").Replace(", ", " ");
    }

    /**
     * @brief 프로젝트에서 사용하는 위치 범위로 변환된 Vector3 값을 리턴
     * @param _src 위치 값
     * @returns Vector3 value
     * 0 <= x <360, -90 <= y <= 90
     */
    static public Vector3 ToAngleRange(Vector3 _src)
    {
        return ProjCtrl.ToAngleRange(_src.x, _src.y, _src.z);
    }

    /**
     * @brief 프로젝트에서 사용하는 위치 범위로 변환된 Vector3 값을 리턴
     * @param _x X 위치 값
     * @param _y Y 위치 값
     * @param _z Z 위치 값
     * @returns Vector3 value
     * 0 <= x <360, -90 <= y <= 90
     */
    static public Vector3 ToAngleRange(float _x, float _y, float _z)
    {
        float x = (((_x % 360.0f) + 360.0f) % 360.0f); //0 <= x <360
        float y = (((_y % 360.0f) + 360.0f) % 360.0f); //0 <= y <360

        if (270.0f <= y) y = y - 360.0f;
        else if (90.0f < y)
        {
            y = 180.0f - y;
            x = (x + 180.0f) % 360.0f;
        }

        if (360.0f <= x) x -= 360.0f;
        if (x < 0.0f) x += 360.0f;

        return new Vector3(x, y, _z);
    }

    /**
     * @brief 프로젝트 컨텐츠 편집 여부 리턴
     * @returns 편집 여부
     */
    static public bool IsEdit()
    {
        return ProjCtrl.Instance.Edited;
    }
}
