---

# Virtual Smith, VR 컨텐츠 제작 툴 '버추얼스미스(APP)'

 * 스마트폰에 설치하는 가벼운 앱으로 장소에 구애받지 않고 누구나 쉽고 저렴하게 VR UCC를 만들 수 있는 툴입니다.
 
 * 이용자가 원하는 곳에 정보를 삽입하고 이를 선택 또는 트레킹으로 정보를 확인할 수 있습니다.
 
---

## 기능들

 * 프로젝트 열기 / 만들기

 * 텍스트 정보 보기 / 입력하기

 * 이미지 정보 보기 / 입력하기

 * 씬 이동하기 / 입력하기
 
---

## 기기 등록하기

 * 개발중인 '버추얼스미스(APP)'를 설치하려면 사용하는 아이폰을 개발 기기로 등록해야 합니다.

 * 개발 기기로 등록하기 위해서는 폰의 고유한 UDID 번호가 필요합니다. iPhone의 UDID는 아래 방법으로 확인할 수 있습니다.

 1. 아이폰에서 Sapari ( 사파리 ) 브라우저를 실행합니다.

 2. 주소창에 'udid.io'를 입력하고 나오는 화면에서, 'Tap to find UDID' 버튼을 누르면 UDID 번호가 보입니다.

 3. 이 번호를 쉽게 복사하려면 번호 위를 손가락으로 살짝 눌러주세요. 

 * UDID를 개발자 연락처로 보내주세요.
 
---

## 앱 설치하기

 * https://bitbucket.org/studio903/vrsmith/downloads/VirtualSmith.ipa 에서 설치 파일을 다운로드 합니다.

 * iTunes를 실행시키고, ipa 파일을 기기 항목으로 드래그 하면 설치 됩니다.
 
---

## 개발자 정보

 * 김지수

 * kind.jamie@gmail.com

 ---

©TECHNOBLOOD INC. ALL RIGHTS RESERVED.

---
