﻿namespace AlertView.iOS
{
    public enum AlertStyle
    {
        None = 0,
        Alert = 1,
        ActionSheet = 2
    }
}
