﻿namespace AlertView.iOS
{
    public enum ButtonStyle 
    {
        None = 0,
        Default = 1,
        Cancel = 2,
        Destructive = 3
    }
}