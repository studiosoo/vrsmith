/**
* @file ProjListViewCtrl.cs
* @date 2018/07/23
* @author KIMJISOO(kind.jamie@gmail.com)
* @brief 프로젝트 리스트 뷰 컨트롤 스크립트
*/
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using ImageAndVideoPicker;

/**
* @class ProjListViewCtrl
* @date 2018/07/23
* @brief 이미지 어노테이션 컨텐츠 오브젝트 컨트롤
* ProjListView의 컨트롤
*/
public class ProjListViewCtrl : MonoBehaviour {

    //SINGLETON
    static public ProjListViewCtrl Instance = null;    /** 싱글턴 인스턴스 */

    //Menu
    public Button btnDel;                   /** 삭제 버튼 */
    public Button btnNew;                   /** 생성 버튼 */

    //New
    public GameObject NewProjView;          /** 프로젝트 생성 뷰 */
    public InputField inpName;              /** 이름 입력 필드 */
    public Button btnBack;                  /** 취소 버튼 */
    public Button btnDone;                  /** 확인 버튼 */

    /**
     * @brief 앱 구동시, 싱글턴 인스턴스 설정
     */
    void Awake()
    {
        if (Instance == null) Instance = this;
    }

    /**
     * @brief 뷰와 필드 초기화, 리스너 등록
     */
	void Start () 
    {
        this.btnDel.onClick.AddListener(OnDelButtonClicked);
        this.btnNew.onClick.AddListener(OnNewButtonClicked);
        this.inpName.onEndEdit.AddListener(delegate { OnNameInputEndEdit(); });
        this.btnBack.onClick.AddListener(OnBackButtonClicked);
        this.btnDone.onClick.AddListener(OnDoneButtonClicked);

        this.NewProjView.SetActive(false);
        this.gameObject.SetActive(true);
	}

    //DELEGATE
    /**
     * @brief 이미지 설정 버튼 클릭 콜백
     * 임시 경로에 이미지 저장 후 기존 이미지 설정 함수 콜
     */
    private void OnFinishedImagePicker(string message)
    {
        if (LoadTextureFromImagePicker.IsLoaded())
        {
            int width = LoadTextureFromImagePicker.GetLoadedTextureWidth();
            int height = LoadTextureFromImagePicker.GetLoadedTextureHeight();
            bool mipmap = true;
            Texture2D texture = LoadTextureFromImagePicker.GetLoadedTexture(message, width, height, mipmap);
            if (texture)
            {
                string path = Path.Combine(MainCtrl.ROOT_DIR, ProjCtrl.DateTime() + ".JPG");
                LoadTextureFromImagePicker.SaveToLocalFile(path, texture);
                this.OnImageSelect(path, ImageOrientation.UP);
                File.Delete(path);
            }
            LoadTextureFromImagePicker.ReleaseLoadedImage();
        }
        else
        {
            LoadTextureFromImagePicker.Release();
        }
    }

    /**
     * @brief 이미지 피커 뷰에서 이미지를 선택
     * 입력한 프로젝트 이름과 선택한 이미지로 새 프로젝트 생성
     * @todo 이미지 선택 오류 보여주기, 파노라마 이미지 여부
     */
    void OnImageSelect(string imgPath, ImageAndVideoPicker.ImageOrientation imgOrientation)
    {
        if (PanoramicViewCtrl.isPanoramic(imgPath) == false)
        {
            //IOSPicker.BrowseImage(false);
            LoadTextureFromImagePicker.ShowPhotoLibrary(gameObject.name, "OnFinishedImagePicker");
            return;
        }

        EffectViewCtrl.Progress(true);

        ProjCtrl.CreateProject(this.inpName.text, imgPath);
        ProjContentViewCtrl.Open(MainCtrl.CurScene, OnEndOpen);
    }

    /**
     * @brief 삭제 버튼 클릭
     */
    void OnDelButtonClicked()
    {
        if (this.NewProjView.activeSelf) return;

        string path = this.gameObject.GetComponent<ProjListCtrl>().SelectedFilePath();
        if (path.Equals(string.Empty)) return;
        File.Delete(path);

        this.gameObject.GetComponent<ProjListCtrl>().BuildList();
    }

    /**
     * @brief 생성 버튼 클릭
     */
    void OnNewButtonClicked()
    {
        if (this.NewProjView.activeSelf) return;

        this.NewProjView.SetActive(true);
        this.inpName.text = "";
        this.inpName.ActivateInputField();
    }

    /**
     * @brief 프로젝트 이름 입력 뷰 DONE 버튼 클릭
     */
    void OnNameInputEndEdit()
    {
        if (this.inpName.wasCanceled) return;

        OnDoneButtonClicked();
    }

    /**
     * @brief 프로젝트 이름 입력 뷰 취소 버튼 클릭 
     */
    void OnBackButtonClicked()
    {
        this.NewProjView.SetActive(false);
    }

    /**
     * @brief 프로젝트 이름 입력 뷰 DONE 버튼 클릭
     */
    void OnDoneButtonClicked()
    {
        if (this.inpName.text.Length == 0) return;

        this.NewProjView.SetActive(false);
        //IOSPicker.BrowseImage(false);
        LoadTextureFromImagePicker.ShowPhotoLibrary(gameObject.name, "OnFinishedImagePicker");
    }

    /**
     * @brief 리스트에서 프로젝트 선택
     * @param _directory 프로젝트 파일 경로
     * @param _name 프로젝트 파일 이름, 확장자 포함
     * 프로젝트 열기
     */
    void _OnListItemClicked(string _directory, string _name)
    {
        if (this.NewProjView.activeSelf) return;

        EffectViewCtrl.Progress(true);

        ProjCtrl.OpenProject(Path.Combine(_directory, _name));
        ProjContentViewCtrl.Open(MainCtrl.CurScene, OnEndOpen);
    }

    /**
     * @brief 텍스쳐 설정 종료, 콜백함수
     */
    void OnEndOpen()
    {
        this.gameObject.SetActive(false);
        this.NewProjView.SetActive(false);
    }

    //STATIC
    /**
     * @brief 리스트에서 프로젝트 선택
     * @param _directory 프로젝트 파일 경로
     * @param _name 프로젝트 파일 이름, 확장자 포함
     * 프로젝트 열기
     */
    static public void OnListItemClicked(string _directory, string _name)
    {
        ProjListViewCtrl.Instance._OnListItemClicked(_directory, _name);
    }
}
