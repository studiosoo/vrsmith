﻿/**
* @file TextAnnotObjCtrl.cs
* @date 2018/07/23
* @author KIMJISOO(kind.jamie@gmail.com)
* @brief 텍스트 어노테이션 오브젝트 컨트롤 스크립트
*/
using UnityEngine;

/**
* @class TextAnnotObjCtrl
* @date 2018/07/23
* @brief 텍스트 어노테이션 오브젝트 컨트롤
* TextAnnotObj의 컨트롤
*/
public class TextAnnotObjCtrl : MonoBehaviour 
{
    //CONTENT
    public delegate void _HideAnnotContent(GameObject _obj);
    public _HideAnnotContent HideAnnotContent;  /** 어노테이션 내용 가리기 */
    public delegate void _ShowAnnotContent(GameObject _obj, string _pos, string _name, string _desc);
    public _ShowAnnotContent ShowAnnotContent;  /** 어노테이션 내용 보이기 */
    bool Collided = false;              /** AnnotAimView와 충돌 여부 */
    public bool OnEdit = false;

    //INFO
    public int Idx;                     /** 인텍스, 어노테이션 종류별로 유일함, 0~ */
    public string Name;                 /** 이름 */
    public Vector3 Angle;               /** 파노라믹 뷰 위의 위치, 0 <= x < 360 -90 <= y <= 90 */
    public string CreatedTime;          /** 생성 시간 */
    public string ModifiedTime;         /** 수정된 시간 */
    public string Pos;                  /** 텍스트 어노테이션 내용 표시 위치 선택 값, 0 ~ 4 */
    public string Desc;                 /** 설명 */

    //PUBLIC
    /**
     * @brief Content 표시 설정
     * @param _collided Annot Aim 충돌 여부, 조준됨
     */
    public void OnCollided(bool _collided)
    {
        //Debug.Log(this.OnEdit.ToString() + " " + this.Collided.ToString() + " " + _collided.ToString());

        if (this.OnEdit) _collided = true;
        if (_collided && !this.Collided)
        {
            ShowAnnotContent(this.gameObject, this.Pos, this.Name, this.Desc);
        }
        if (!_collided && this.Collided)
        {
            HideAnnotContent(this.gameObject);
        }
        this.Collided = _collided;
    }

    /**
     * @brief 어노테이션 정보 설정
     * @param _idx 인텍스, 어노테이션 종류별로 유일함, 0~
     * @param _name 이름
     * @param _angle 파노라믹 뷰 위의 위치
     * @param _ctime 생성 시간
     * @param _mtime 수정된 시간
     * @param _desc 설명
     */
    public void Set(int _idx, Vector3 _angle, string _pos, string _name, string _ctime, string _mtime, string _desc)
    {
        this.Collided = false;

        this.Idx = _idx;
        this.Angle = _angle;
        this.Pos = _pos;
        this.Name = _name;
        this.CreatedTime = _ctime;
        this.ModifiedTime = _mtime;
        this.Desc = _desc;
    }

}

