/**
 * @file PanoramicViewCtrl.cs 
 * @date 2018/07/23 
 * @author KIMJISOO(kind.jamie@gmail.com)
 * @brief 파노라믹 뷰 컨트롤 스크립트
 */
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Threading;

/**
 * @class PanoramicViewCtrl
 * @date 2018/07/23
 * @brief 파노라믹 뷰 컨트롤
 * MainPanoramicView의 컨트롤
 * 파노라믹 뷰는 고정시키고 카메라가 회전함
 */
public class PanoramicViewCtrl : MonoBehaviour {

    //STATIC
    static public Ray AnnotAim;                         /** 어노테이션 조준 */

    //SINGLETON
    static public PanoramicViewCtrl Instance = null;    /** 싱글턴 인스턴스 */

    Thread thread = null;                               /** 텍스쳐 로딩 스레드 */
    bool TaskDone = false;                              /** 텍스쳐 로딩 작업 완료 여부 */
    System.Action callback1 = null;                     /** 텍스쳐 로딩 콜백 1 */
    System.Action callback2 = null;                     /** 텍스쳐 로딩 콜백 2 */

    /**
     * @brief 앱 구동시, 싱글턴 인스턴스 설정
     */
    void Awake()
    {
        if (Instance == null) Instance = this;
    }

    /**
     * @brief 어노테이션 조준 오브젝트 설정
     */
    void Start()
    {
        PanoramicViewCtrl.AnnotAim = new Ray(Camera.main.transform.position, Vector3.forward);
    }

    /**
     * @brief 터치 이벤트 확인, 텍스쳐 로딩 작업 완료 처리
     */
    void Update()
    {
        if (Input.touchCount == 1 && EventSystem.current.IsPointerOverGameObject(0) == false)
        {
            Touch touch  = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved)
            {
                this.OnMoved(touch);
            }
        }
        if (thread != null && this.TaskDone)
        {
            if (this.callback1 != null) this.callback1();
            if (this.callback2 != null) this.callback2();
            this.callback1 = null;
            this.callback2 = null;
            thread.Abort();
            thread = null;
        }   
    }

    /**
    * @brief 카메라 회전, 어노테이션 타겟팅 확인
    */
    void OnMoved(Touch _touch)
    {
        Camera.main.transform.Rotate(Vector3.right, _touch.deltaPosition.y / 4.0f);
        Camera.main.transform.RotateAround(Vector3.zero, Vector3.down, _touch.deltaPosition.x / 4.0f);
        PanoramicViewCtrl.AnnotAim.direction = Camera.main.transform.forward;

        this.RaycastAll();
    }

    /**
     * @brief 활성화시, 뷰 상태 초기화
     */
    void OnEnable()
    {
        Camera.main.transform.rotation = Quaternion.identity;
    }

    /**
     * @brief 등록된 어노테이션 삭제
     */
    void _ClearChildren()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
        transform.DetachChildren();
    }

    /**
    * @brief 어노테이션 조준 여부 확인
    */
    void RaycastAll()
    {
        List<TextAnnotObjCtrl> txts = new List<TextAnnotObjCtrl>(
            this.transform.GetComponentsInChildren<TextAnnotObjCtrl>());
        List<ImageAnnotObjCtrl> imgs = new List<ImageAnnotObjCtrl>(
            this.transform.GetComponentsInChildren<ImageAnnotObjCtrl>());
        List<LinkAnnotObjCtrl> lnks = new List<LinkAnnotObjCtrl>(
            this.transform.GetComponentsInChildren<LinkAnnotObjCtrl>());

        List<RaycastHit> hits = new List<RaycastHit>(
            Physics.RaycastAll(PanoramicViewCtrl.AnnotAim));
        //Debug.Log("RaycastAll hits " + hits.Count.ToString() + " t " + txts.Count.ToString());
        foreach(RaycastHit hit in hits)
        {
            TextAnnotObjCtrl txt = hit.collider.GetComponent<TextAnnotObjCtrl>();
            if (txt != null)
            {
                txt.OnCollided(true);
                txts.Remove(txt);
            }

            ImageAnnotObjCtrl img = hit.collider.GetComponent<ImageAnnotObjCtrl>();
            if (img != null)
            {
                img.OnCollided(true);
                imgs.Remove(img);
            }

            LinkAnnotObjCtrl lnk = hit.collider.GetComponent<LinkAnnotObjCtrl>();
            if (lnk != null)
            {
                lnk.OnCollided(true);
                lnks.Remove(lnk);
            }
        }

        foreach(TextAnnotObjCtrl txt in txts)
        {
            txt.OnCollided(false);
        }
        foreach (ImageAnnotObjCtrl img in imgs)
        {
            img.OnCollided(false);
        }
        foreach (LinkAnnotObjCtrl lnk in lnks)
        {
            lnk.OnCollided(false);
        }
    }

    /**
     * @brief 파노라믹 뷰 텍스쳐 설정
     * @param _path 이미지 파일 경로
     * @param _callback1 작업 완료후 호출할 함수
     * @param _callback2 작업 완료후 호출할 함수
     * 텍스쳐 로딩 스레드 생성
     */
    void _SetTexture(string _path, System.Action _callback1, System.Action _callback2)
    {
        if (thread == null)
        {
            this.TaskDone = false;
            this.callback1 = _callback1;
            this.callback2 = _callback2;
            thread = new Thread(() => UpdateThread(_path));
            thread.Start();
        }
    }

    /**
     * @brief 파노라믹 뷰 텍스쳐 로딩 진행
     * @param _path 이미지 파일 경로
     */
    void UpdateThread(string _path)
    {
        if (System.IO.File.Exists(_path) == false)
        {
            Debug.Log("Not Exist [" + _path + "]");
            return;
        }

        byte[] fileData = System.IO.File.ReadAllBytes(_path);
        Texture2D texture = new Texture2D(2, 2);
        if (texture.LoadImage(fileData))
        {
            Texture tex = PanoramicViewCtrl.Instance.GetComponent<Renderer>().material.GetTexture("_Tex");
            PanoramicViewCtrl.Instance.GetComponent<Renderer>().material.
                             SetTexture("_Tex", CubemapConverter.Convert(texture));
            Destroy(tex);
            Debug.Log("SUCCESS: " + _path);
        }
        else
        {
            Debug.Log("FAIL: " + _path);
        }
        this.TaskDone = true;
    }

    //STATIC
    /**
     * @brief MainPanoramicView 텍스쳐 로딩 진행 여부
     * @returns 진행 여부
     */
    static public bool OnThread()
    {
        return !PanoramicViewCtrl.Instance.TaskDone;
    }

    /**
     * @brief MainPanoramicView 활성화
     * @parma _active 상태
     */
    static public void SetActive(bool _active)
    {
        PanoramicViewCtrl.Instance.gameObject.SetActive(_active);
    }

    /**
     * @brief 등록된 어노테이션 삭제
     */
    static public void ClearAnnots()
    {
        PanoramicViewCtrl.Instance._ClearChildren();
    }

    /**
     * @brief Cubemap Texture 설정
     * @parma _path 파노라마 이미지 경로
     */
    static public void SetTexture(string _path, System.Action _callback1, System.Action _callback2) 
    {
        PanoramicViewCtrl.Instance._SetTexture(_path, _callback1, _callback2);
	}

    /**
     * @brief _angle 위치에 어노테이션 브라켓을 표시 하기
     * @parma _obj 어노테이션의 게임 오브젝트 인스턴스
     * @parma _angle 어노테이션의 위치
     */
    static public void Set(GameObject _obj, Vector3 _angle)
    {
        Vector3 dir = Quaternion.AngleAxis(_angle.x, Vector3.up) * 
                                Quaternion.AngleAxis(-_angle.y, Vector3.right) * 
                                Vector3.forward;
        Ray ray = new Ray(PanoramicViewCtrl.Instance.transform.position, dir.normalized);
        _obj.transform.position = ray.GetPoint(PanoramicViewCtrl.Instance.GetComponent<SphereCollider>().radius *
                                                  PanoramicViewCtrl.Instance.transform.localScale.x);
        //Debug.Log("Set " + _obj.ToString() + " angle " + _angle.ToString() + " dir " + dir.ToString() + " pos " + _obj.transform.position.ToString());
        _obj.transform.rotation = Quaternion.identity;
        _obj.transform.Rotate(new Vector3(0.0f, _angle.x, 0.0f));
        _obj.transform.Rotate(new Vector3(-_angle.y, 0.0f, 0.0f));
        _obj.transform.SetParent(PanoramicViewCtrl.Instance.transform);
    }

    /**
     * @brief 어노테이션을 조준하도록 카메라를 회전시킴
     * @parma _obj 조준할 어노테이션의 게임 오브젝트 인스턴스
     */
    static public void Rotate(GameObject _obj)
    {
        Camera.main.transform.LookAt(_obj.transform);
        PanoramicViewCtrl.AnnotAim.direction = Camera.main.transform.forward;
        PanoramicViewCtrl.Raycast();
    }

    /**
     * @brief 조준점과 어노테이션의 충돌 여부를 확인함
     */
    static public void Raycast()
    {
        PanoramicViewCtrl.Instance.RaycastAll();
    }

    /**
     * @brief 카메라(조준점)가 향한 위치를 리턴
     * @returns 파노라믹 뷰 상의 위치 값
     */
    static public Vector3 Angle()
    {
        Vector3 rot = Camera.main.transform.rotation.eulerAngles;
        return new Vector3(rot.y, 360.0f - rot.x, rot.z);
    }

    /**
     * @brief 경로의 이미지가 Cubemap으로 변환할 수 있는 파노라마 이미지인지 확인함
     * @returns 파노라마 이미지 여부
     */
    static public bool isPanoramic(string _path)
    {
        if (System.IO.File.Exists(_path) == false)
        {
            Debug.Log("Not Exist [" + _path + "]");
            return false;
        }

        byte[] fileData = System.IO.File.ReadAllBytes(_path);
        Texture2D tex = new Texture2D(2, 2);
        if (tex.LoadImage(fileData))
        {
            if (tex.height < tex.width && tex.width == (tex.height * 2))
            {
                return true;
            }
        }
        return false;
    }
}
