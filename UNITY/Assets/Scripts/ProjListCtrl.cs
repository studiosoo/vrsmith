﻿/**
 * @file ProjListCtrl.cs 
 * @author Endgame Studios(listview@endgamestudios.com)
 * @brief 프로젝트 리스트 컨트롤 스크립트
 * ListView for Unity UI
 * Copyright © 2014 Endgame Studios
 * http://www.endgamestudios.com      
 */
#if UNITY_WEBPLAYER || UNITY_WP8 || UNITY_WP8_1 || UNITY_WSA || UNITY_WSA_8_0 || UNITY_WSA_8_1
#define API_UNAVAILABLE
#endif

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Endgame;

/**
 * @class ProjListCtrl
 * @date 2018/07/23
 * @brief 프로젝트 리스트 컨트롤
 * ProjListView의 컨트롤
 */
public class ProjListCtrl : MonoBehaviour
{
    public ListView ListView;       /** 리스트 뷰 */

    List<string> currentDirectoryChain = new List<string>(); /** 현재 디렉토리의 항목 */

	string currentDirectory         /** 현재 디렉토리 */
	{
		get
		{
			string result = string.Empty;

			foreach (string directoryName in this.currentDirectoryChain)
			{
				result += directoryName + "/";
			}

			return result;
		}
	}

    abstract class DirectoryEntry
    {
        public string Name = null;
        public System.DateTime LastModifiedTime = System.DateTime.Now;
        public long Size = 0;

        public DirectoryEntry(string name, System.DateTime lastModifiedTime, long size)
        {
            this.Name = name;
            this.LastModifiedTime = lastModifiedTime;
            this.Size = size;
        }
    }

    class Directory : DirectoryEntry
    {
        public Directory(string name, System.DateTime lastModifiedTime)
            : base(name, lastModifiedTime, 0)
        {
        }
    }

    class File : DirectoryEntry
    {
        public File(string name, System.DateTime lastModifiedTime, long size)
            : base(name, lastModifiedTime, size)
        {
        }
    }

    class ListViewItemComparer : IComparer
    {
        int columnIndex = 0;

        public ListViewItemComparer()
        {
        }

        public ListViewItemComparer(int columnIndex)
        {
            this.columnIndex = columnIndex;
        }

        public int Compare(object object1, object object2)
        {
            ListViewItem listViewItem1 = object1 as ListViewItem;
            ListViewItem listViewItem2 = object2 as ListViewItem;
            string text1 = listViewItem1.SubItems[this.columnIndex].Text;
            string text2 = listViewItem2.SubItems[this.columnIndex].Text;
            return string.Compare(text1, text2);
        }
    }

	void Start()
	{
#if UNITY_IOS || UNITY_STANDALONE_OSX
        this.currentDirectoryChain.Add(MainCtrl.PROJ_DIR);
#else
    	this.currentDirectoryChain.Add("C:");
#endif

        this.AddColumns();

        // Add items.
        this.AddFiles();

        this.ListView.ItemActivate += OnItemActivated;
		this.ListView.ColumnClick += OnColumnClick;
        //750
		this.ListView.Columns[0].Width = 350;
		this.ListView.Columns[1].Width = 280;
		this.ListView.Columns[2].Width = 120;
	}

    //DELEGATE
    void OnItemActivated(object sender, System.EventArgs e)
    {
        ListView listView = (ListView)sender;
        ListViewItem item = listView.SelectedItems[0];
        if (item.Tag is File)
        {
            File file = item.Tag as File;
            ProjListViewCtrl.OnListItemClicked(this.currentDirectory, file.Name);
        }
        else if (item.Tag is Directory)
        {
            Directory directory = item.Tag as Directory;
            this.currentDirectoryChain.Add(directory.Name);
            AddFiles();
        }
        else
        {
            // Up one level.
            this.currentDirectoryChain.RemoveAt(this.currentDirectoryChain.Count - 1);
            AddFiles();
        }
    }

    void OnColumnClick(object sender, ListView.ColumnClickEventArgs e)
    {
        ListView listView = (ListView)sender;
        listView.ListViewItemSorter = new ListViewItemComparer(e.Column);
    }

    void AddColumnHeader(string title)
    {
        ColumnHeader columnHeader = new ColumnHeader();
        columnHeader.Text = title;
        this.ListView.Columns.Add(columnHeader);
    }

    void AddColumns()
	{
		this.ListView.SuspendLayout();
		{
			AddColumnHeader("Name");
			AddColumnHeader("Modified");
			AddColumnHeader("Size");
		}
		this.ListView.ResumeLayout();
	}

    void AddFiles()
    {
        System.IO.DirectoryInfo directoryInfo = new System.IO.DirectoryInfo(this.currentDirectory);
        IEnumerable<Directory> subDirectories = GetSubDirectories(directoryInfo);
        IEnumerable<File> files = GetFiles(directoryInfo);

        this.ListView.SuspendLayout();
        {
            this.ListView.Items.Clear();

            // Add the previous folder.
            if (this.currentDirectoryChain.Count > 1)
            {
                AddListViewItem("..", string.Empty, string.Empty, null);
            }

            foreach (Directory subDirectory in subDirectories)
            {
                AddListViewItem(subDirectory.Name, subDirectory.LastModifiedTime.ToString(), string.Empty, subDirectory);
            }

            foreach (File file in files)
            {
                AddListViewItem(file.Name, file.LastModifiedTime.ToString(), GetPrettyFileSizeTextFromInteger(file.Size), file);
            }
        }
        this.ListView.ResumeLayout();
    }

    void AddListViewItem(string _name, string _modified, string _size, object _tag)
    {
        ListViewItem listViewItem = new ListViewItem(_name);
        listViewItem.Tag = _tag;
        listViewItem.SubItems.Add(_modified);
        listViewItem.SubItems.Add(_size);
        listViewItem.UseItemStyleForSubItems = false;
        //listViewItem.SubItems[1].ForeColor = new Color(0.43f, 0.43f, 0.43f);
        //listViewItem.SubItems[2].ForeColor = new Color(0.43f, 0.43f, 0.43f);
        this.ListView.Items.Add(listViewItem);
    }

	IEnumerable<Directory> GetSubDirectories(System.IO.DirectoryInfo parentDirectoryInfo)
	{
		List<Directory> subDirectories = new List<Directory>();

#if !API_UNAVAILABLE

		System.IO.DirectoryInfo[] subDirectoriesInfos = parentDirectoryInfo.GetDirectories();
		foreach (System.IO.DirectoryInfo subDirectoryInfo in subDirectoriesInfos)
		{
			subDirectories.Add(new Directory(subDirectoryInfo.Name, subDirectoryInfo.LastWriteTime));
		}

#else //!API_UNAVAILABLE

    	for (int i = 0; i < 10; i++)
    	{
    		subDirectories.Add(new Directory(string.Format("Placeholder folder {0}", i + 1), System.DateTime.Now));
    	}

#endif //!API_UNAVAILABLE

		return subDirectories;
	}

	IEnumerable<File> GetFiles(System.IO.DirectoryInfo parentDirectoryInfo)
	{
		List<File> files = new List<File>();

#if !API_UNAVAILABLE

		System.IO.FileInfo[] fileInfos = parentDirectoryInfo.GetFiles();
		foreach (System.IO.FileInfo fileInfo in fileInfos)
		{
            if (Path.GetExtension(fileInfo.Name).Contains("bak")) continue;
			files.Add(new File(fileInfo.Name, fileInfo.LastWriteTime, fileInfo.Length));
		}

#else //!API_UNAVAILABLE

    	for (int i = 0; i < 10; i++)
    	{
    		files.Add(new File(string.Format("Placeholder file {0}", i + 1), System.DateTime.Now, Random.Range(2048, 50000000)));
    	}

#endif //!API_UNAVAILABLE

		return files;
	}

	string GetPrettyFileSizeTextFromInteger(long fileSizeInBytes)
	{
		long kilobytes = (fileSizeInBytes / 1024);
		long megabytes = (kilobytes / 1024);
		long gigabytes = (megabytes / 1024);

		if (gigabytes > 0)
		{
			return string.Format("{0} GB", gigabytes);
		}
		else if (megabytes > 0)
		{
			return string.Format("{0} MB", megabytes);
		}
		else if (kilobytes > 0)
		{
			return string.Format("{0} KB", kilobytes);
		}
		else
		{
			return string.Format("{0} bytes", fileSizeInBytes);
		}
	}

    //PUBLIC
    public void BuildList()
    {
        AddFiles();
    }

    public string SelectedFilePath()
    {
        string path = string.Empty;
        if (this.ListView.SelectedItems.Count == 0) return path;

        ListViewItem item = this.ListView.SelectedItems[0];
        if (item != null && item.Tag is File)
        {
            File file = item.Tag as File;
            path = Path.Combine(this.currentDirectory, file.Name);
        }
        return path;
    }
}
