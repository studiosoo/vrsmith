﻿using System;

namespace AlertView.iOS
{
    [Serializable]
    public class ButtonData
    {
        public string title;
        public ButtonStyle style;
        public Action callback;


        public ButtonData(string title, ButtonStyle style, Action callback)
        {
            this.title = title;
            this.style = style;
            this.callback = callback;
        }
    }
}