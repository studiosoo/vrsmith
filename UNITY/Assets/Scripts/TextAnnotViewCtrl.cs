/**
* @file TextAnnotViewCtrl.cs
* @date 2018/07/23
* @author KIMJISOO(kind.jamie@gmail.com)
* @brief 텍스트 어노테이션 뷰 컨트롤 스크립트
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
* @class TextAnnotViewCtrl
* @date 2018/07/23
* @brief 파노라믹 뷰 컨트롤
* ContentView의 컨트롤과 텍스트 어노테이션의 리스트
*/
public class TextAnnotViewCtrl : MonoBehaviour {
    
    //SUBVIEW
    public GameObject LTContentView;    /** Left-Top 텍스트 어노테이션 컨텐츠 뷰 */
    public GameObject RTContentView;    /** Right-Top 텍스트 어노테이션 컨텐츠 뷰 */
    public GameObject LBContentView;    /** Left-Bottom 텍스트 어노테이션 컨텐츠 뷰 */
    public GameObject RBContentView;    /** Right-Bottom 텍스트 어노테이션 컨텐츠 뷰 */
    public GameObject CLContentView;    /** Cloud 텍스트 어노테이션 컨텐츠 뷰 */

    //OBJECTS
    List<GameObject> Annots = null;     /** 이미지 어노테이션 오브젝트 리스트 */
    GameObject TargetObj = null;        /** 컨텐츠 표시 대상 어노테이션 오브젝트 */

    /**
     * @brief 활성화시, 리스트 갱신
     */
    void OnEnable()
    {
        this.LTContentView.SetActive(false);
        this.RTContentView.SetActive(false);
        this.LBContentView.SetActive(false);
        this.RBContentView.SetActive(false);
        this.CLContentView.SetActive(false);

        this.Annots = new List<GameObject>();
    }

    /**
     * @brief 비활성화
     */
    void OnDisable()
    {
        this.LTContentView.SetActive(false);
        this.RTContentView.SetActive(false);
        this.LBContentView.SetActive(false);
        this.RBContentView.SetActive(false);
        this.CLContentView.SetActive(false);
    }

    //PUBLIC
    /**
     * @brief 텍스트 어노테이션 오브젝트 리스트 리턴
     * @returns 게임 오브젝트의 리스트
     */
    public List<GameObject> List()
    {
        return this.Annots;
    }

    /**
     * @brief 인덱스에 해당하는 게임 오브젝트 리턴
     * @param _idx 텍스트 어노테이션 오브젝트 리스트의 인덱스, 0~
     * @returns 게임 오브젝트
     */
    public GameObject GetObject(int _idx)
    {
        return this.Annots[_idx];
    }

    /**
     * @brief 텍스트 어노테이션 오브젝트 리스트 새 인덱스 리턴
     * @returns 인덱스 , 0~
     */
    public int NewIdx()
    {
        if (Annots.Count == 0) return 0;
        GameObject lastObj = Annots[Annots.Count - 1];
        return lastObj.GetComponent<TextAnnotObjCtrl>().Idx + 1;
    }

    /**
     * @brief 텍스트 어노테이션 오브젝트 리스트 갱신
     * @returns 인덱스 , 0~
     */
    public void BuildList()
    {
        if (this.Annots != null)
        {
            foreach (GameObject obj in this.Annots)
            {
                DestroyImmediate(obj);
            }
            this.Annots.Clear();
        }

        this.Annots = ProjCtrl.TextAnnots(MainCtrl.CurScene);

        foreach (GameObject obj in this.Annots)
        {
            TextAnnotObjCtrl annotObject = obj.GetComponent<TextAnnotObjCtrl>();
            if (annotObject)
            {
                annotObject.HideAnnotContent = this.HideAnnotContent;
                annotObject.ShowAnnotContent = this.ShowAnnotContent;
                PanoramicViewCtrl.Set(obj, annotObject.Angle);
                PanoramicViewCtrl.Set(obj, annotObject.Angle);
            }
        }

        this.TargetObj = null;
    }

    /**
     * @brief 텍스트 어노테이션 내용 가리기
     * @param _obj 텍스트 어노테이션의 게임 오브젝트
     */
    public void HideAnnotContent(GameObject _object = null)
    {
        Debug.Log("HideAnnotContent");

        if (_object != null && !GameObject.Equals(this.TargetObj, _object)) return;
        this.TargetObj = null;

        this.LTContentView.SetActive(false);
        this.RTContentView.SetActive(false);
        this.LBContentView.SetActive(false);
        this.RBContentView.SetActive(false);
        this.CLContentView.SetActive(false);
    }

    /**
     * @brief 텍스트 어노테이션 내용 보이기
     * @param _obj 텍스트 어노테이션의 게임 오브젝트
     * @param _name 어노테이션 이름
     * @param _desc 어노테이션 설명
     */
    public void ShowAnnotContent(GameObject _object, string _pos, string _name, string _desc)
    {
        Debug.Log("ShowAnnotContent " + _name);

        this.TargetObj = _object;

        this.LTContentView.SetActive(false);
        this.RTContentView.SetActive(false);
        this.LBContentView.SetActive(false);
        this.RBContentView.SetActive(false);
        this.CLContentView.SetActive(false);

        GameObject obj = null;
        if (_pos.ToLower() == TextAnnotContentObjCtrl.LTPOS) obj = this.LTContentView;
        else if (_pos.ToLower() == TextAnnotContentObjCtrl.RTPOS) obj = this.RTContentView;
        else if (_pos.ToLower() == TextAnnotContentObjCtrl.LBPOS) obj = this.LBContentView;
        else if (_pos.ToLower() == TextAnnotContentObjCtrl.RBPOS) obj = this.RBContentView;
        else if (_pos.ToLower() == TextAnnotContentObjCtrl.CLPOS) obj = this.CLContentView;
        else obj = this.CLContentView;

        obj.GetComponent<TextAnnotContentObjCtrl>().Set(_name, _desc);
        obj.SetActive(true);
    }

}
