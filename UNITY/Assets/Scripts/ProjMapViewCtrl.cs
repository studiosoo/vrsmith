﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProjMapViewCtrl : MonoBehaviour {

    public RectTransform MapView;

    //SINGLETON
    static public ProjMapViewCtrl Instance = null;    /** 싱글턴 인스턴스 */

    public Text txtName;                    /** 프로젝트 이름 필드 */
    public Button btnBack;                  /** 닫기 버튼 */
    public Button btnSave;                  /** 저장 버튼 */

    void Awake()
    {
        if (Instance == null) Instance = this;
    }

	void Start()
	{
        this.btnBack.onClick.AddListener(OnBackButtonClicked);
        this.btnSave.onClick.AddListener(OnSaveButtonClicked);
        this.btnSave.gameObject.SetActive(false);
	}

    void OnBackButtonClicked()
    {
        
    }

    void OnSaveButtonClicked()
    {
    }
}
