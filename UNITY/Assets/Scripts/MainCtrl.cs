﻿/**
 * @file MainCtrl.cs 
 * @date 2018/07/23 
 * @author KIMJISOO(kind.jamie@gmail.com)
 * @brief 매인 컨트롤 스크립트
 */
using System.IO;
using UnityEngine;
using UnityEngine.UI;

/**
 * @class MainCtrl
 * @date 2018/07/23
 * @brief 매인 컨트롤
 */
public class MainCtrl : MonoBehaviour
{
    //DEFINES
    static public string ROOT_DIR = "";     /** SANDBOX 경로 */
    static public string PROJ_DIR = "";     /** 프로젝트 파일 경로 */
    static public string TEMP_DIR = "";     /** TEMP 경로 */
    //DEFINES

    public Text FooterText;                 /** 버전, 권리 정보 표시 필드 */

    static public int CurScene = 0;         /** 현재 보여지는 Scene 인덱스 */

    /**
     * @brief 앱 구동시, 경로 초기화
     */
    void Awake()
    {
        MainCtrl.ROOT_DIR = Application.persistentDataPath;
        MainCtrl.PROJ_DIR = Path.Combine(Application.persistentDataPath, "PROJECTS");
        if (Directory.Exists(MainCtrl.PROJ_DIR) == false)
        {
            Directory.CreateDirectory(MainCtrl.PROJ_DIR);
        }

        MainCtrl.TEMP_DIR = Path.Combine(Application.persistentDataPath, "TEMP");
        if (Directory.Exists(MainCtrl.TEMP_DIR) == false)
        {
            Directory.CreateDirectory(MainCtrl.TEMP_DIR);
        }

        this.FooterText.text = "VIRTUAL SMITH " + 
            Application.version +
            "  ©TECHNOBLOOD INC. ALL RIGHTS RESERVED.\n";
    }
}
