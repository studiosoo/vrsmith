﻿/**
 * @file AnnotListCtrl.cs 
 * @date 2018/07/23 
 * @author KIMJISOO(kind.jamie@gmail.com)
 * @brief 어노테이션 리스트 컨트롤 스크립트
 */
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Endgame;

/**
 * @class AnnotListCtrl
 * @date 2018/07/23
 * @brief 어노테이션 리스트 컨트롤
 * AnnotListView의 리스트 컨트롤
 */
public class AnnotListCtrl : MonoBehaviour
{
    //DEFINES
    public const string EDIT_KEY = "Edit";      /** 리스트 아이템 이미지의 편집 이미지 키 값 */
    public const string TEXT_KEY = "Text";      /** 리스트 아이템 이미지의 텍스트 어노테이션 키 값 */
    public const string IMAGE_KEY = "Image";    /** 리스트 아이템 이미지의 이미지 어노테이션 키 값 */
    public const string LINK_KEY = "Link";      /** 리스트 아이템 이미지의 링크 어노테이션 키 값 */
    //DEFINES

    //EXTERNAL VIEW
    public GameObject AnnotEditView;            /** 어노테이션 편집 뷰 */
    public GameObject TextAnnotView;            /** 텍스트 어노테이션 뷰, 텍스트 어노테이션 오브젝트 리스트 참조 */
    public GameObject ImageAnnotView;           /** 이미지 어노테이션 뷰, 이미지 어노테이션 오브젝트 리스트 참조 */
    public GameObject AnnotView;                /** 어노테이션 뷰, 링크 어노테이션 오브젝트 리스트 참조 */
    //EXTERNAL VIEW

    /**
     * @class ItemData
     * @date 2018/07/23
     * @brief 리스트 아이템의 Tag에 설정할 아이템 정보
     */
    class ItemData
    {
        public string ImageKey;                 /** 리스트 아이템의 이미지 키 값, {"Text", "Image"} */
        public int Idx;                         /** 인텍스, 어노테이션 종류별로 유일함, 0~ */
        public string Name;                     /** 이름 */
        public Vector3 Angle;                   /** 파노라믹 뷰 위의 위치, 0 <= x < 360 -90 <= y <= 90 */
        public string CreatedTime;              /** 생성 시간 */
        public string ModifiedTime;             /** 수정된 시간 */
        public GameObject Object;               /** 어노테이션 오브젝트의 게임 오브젝트 */
    }

    const int EDIT_COLUMN = 2;                  /** 리스트 아이템에서 편집 버튼의 위치, {NAME, ANGLE, EDIT} */
    const string FILTER_ALL = "All";            /** 리스트에 모두 표시하는 필터 값 */
    const string FILTE_TEXT = "Text";           /** 리스트에 텍스트 어노테이션만 표시하는 필터 값 */    
    const string FILTE_IMAGE = "Image";         /** 리스트에 이미지 어노테이션만 표시하는 필터 값 */ 
    const string FILTE_LINK = "Link";         /** 리스트에 이미지 어노테이션만 표시하는 필터 값 */ 
    string[] FILTERS = { FILTER_ALL, FILTE_TEXT, FILTE_IMAGE, FILTE_LINK }; /** 리스트에 표시 할 어노테이션 종류 리스트 */ 
    int CUR_FILTER = 0;                         /** 현재 표시할 어노테이션 종류 리스트 값 */

    const string SORT_NAME = "NAME";            /** 이름 기준 정렬 */
    int ORDER_NAME = 0;                         /** 현재 이름 기준 정렬 차순 값, 0: 오름차순 1:내림차순 */
    const string SORT_TIME = "TIME";            /** 편집 시간 기준 정렬 */
    int ORDER_TIME = 0;                         /** 현재 편집 시간 기준 정렬 차순 값, 0: 오름차순 1:내림차순 */
    string CUR_SORT = SORT_NAME;                /** 현재 정렬 기준 값, {"NAME", "TIME"} */
    int CUR_ORDER = 0;                          /** 현재 정렬 차순 값, 0: 오름차순 1:내림차순 */

    //MENU
    public Button btnType;                      /** 리스트 필터 종류 버튼 */
    public Sprite AllAnnotImage;                /** 리스트에 모두 표시하는 필터 종류 이미지 */
    public Sprite TextAnnotImage;               /** 텍스트 어노테이션만 표시하는 필터 종류 이미지 */
    public Sprite ImageAnnotImage;              /** 이미지 어노테이션만 표시하는 필터 종류 이미지 */
    public Sprite LinkAnnotImage;               /** 링크 어노테이션만 표시하는 필터 종류 이미지 */
    public Button btnName;                      /** 이름 정렬 버튼 */
    public Button btnTime;                      /** 시간 정렬 버튼 */

    //List
    public ListView ListView;                   /** 리스트 뷰 */
    ImageList Images;                           /** 리스트 아이템 이미지 리스트 */
    public Button AnnotEditButtonObj;           /** 편집 버튼 오브젝트 */
    public Sprite AnnotEditButtonImage;         /** 편집 버튼 이미지 */
	
    /**
     * @brief 뷰와 필드 초기화, 리스너 등록, 리스트 초기화
     */
	void Start()
    {
        this.btnType.GetComponent<Image>().sprite = AllAnnotImage;
        this.btnType.onClick.AddListener(OnTypeButtonClicked);
        this.btnName.onClick.AddListener(OnNameButtonClicked);
        this.btnTime.onClick.AddListener(OnTimeButtonClicked);

        this.AddColumns();

        this.ListView.ItemBecameVisible += this.OnItemBecameVisible;
        this.ListView.ItemActivate += OnItemActivated;

        //Image.Width = 90
		this.ListView.Columns[0].Width = 370;
		this.ListView.Columns[1].Width = 200;
		this.ListView.Columns[2].Width = 90;

        this.Images = new ImageList();
        this.Images.ImageSize = new Vector2(90, 90);

        this.Images.Images.Add(EDIT_KEY, AnnotEditButtonImage);
        this.Images.Images.Add(TEXT_KEY, TextAnnotImage);
        this.Images.Images.Add(IMAGE_KEY, ImageAnnotImage);
        this.Images.Images.Add(LINK_KEY, LinkAnnotImage);

        this.ListView.SmallImageList = this.Images;
	}

    /**
     * @brief 활성화시, 리스트 갱신
     */
    void OnEnable()
    {
        this.BuildList(this.FILTERS[this.CUR_FILTER], this.CUR_SORT, this.CUR_ORDER);
    }

    //DELEGATE
    /**
     * @brief 닫기 버튼 클릭, 
     */
    void OnBackButtonClicked()
    {
        this.gameObject.SetActive(false);
    }

    /**
     * @brief 리스트 필터 종류 버튼 클릭
     */
    void OnTypeButtonClicked()
    {
        this.CUR_FILTER = (this.CUR_FILTER + 1) % this.FILTERS.Length;
        this.btnType.GetComponent<Image>().sprite = this.CUR_FILTER == 0 ? AllAnnotImage : 
            this.CUR_FILTER == 1 ? TextAnnotImage : 
            this.CUR_FILTER == 2 ? ImageAnnotImage : LinkAnnotImage;

        this.BuildList(this.FILTERS[this.CUR_FILTER], this.CUR_SORT, this.CUR_ORDER);
    }

    /**
     * @brief 이름 순 정렬 버튼 클릭
     */
    void OnNameButtonClicked()
    {
        this.ORDER_NAME = this.ORDER_NAME == 0 ? 1 : 0;
        this.BuildList(this.FILTERS[this.CUR_FILTER], SORT_NAME, this.ORDER_NAME);
    }

    /**
     * @brief 시간 순 정렬 버튼 클릭
     */
    void OnTimeButtonClicked()
    {
        this.ORDER_TIME = this.ORDER_TIME == 0 ? 1 : 0;
        this.BuildList(this.FILTERS[this.CUR_FILTER], SORT_TIME, this.ORDER_TIME);
    }

    /**
     * @brief 리스트 아이템이 뷰 안에 배치 됨
     * @param _item 뷰 안에 들어온 리스트 뷰 아이템
     * 편집 버튼의 리스너를 등록한다.
     */
    void OnItemBecameVisible(ListViewItem _item)
    {
        if (_item == null) return;

        var subItem = _item.SubItems[EDIT_COLUMN];
        if (subItem != null)
        {
            Button btnEdit = Button.Instantiate(this.AnnotEditButtonObj) as Button;
            btnEdit.onClick.AddListener(delegate { OnEditButtonClicked(_item); });
            subItem.CustomControl = btnEdit.transform as RectTransform;
        }
    }

    /**
     * @brief 리스트 아이템이 선택(더블클릭) 됨
     * @param _sender 리스트 뷰
     * @param _e
     * 리스트 뷰 아이템의 어노테이션 오브젝트가 조준 되도록 파노라믹 뷰를 회전 시킴
     */
    void OnItemActivated(object _sender, System.EventArgs _e)
    {
        ListView listView = (ListView)_sender;
        this.RotateToItem(listView.SelectedItems[0]);
    }

    /**
     * @brief 어노테이션 편집 버튼 클릭
     * @param _item 리스트 아이템
     */
    void OnEditButtonClicked(ListViewItem _item)
    {
        if (_item != null)
        {
            ItemData itemData = _item.Tag as ItemData;

            if (itemData.ImageKey == TEXT_KEY)
            {
                this.AnnotEditView.GetComponent<AnnotEditViewCtrl>().
                    EditTextAnnot(this.TextAnnotView.GetComponent<TextAnnotViewCtrl>().GetObject(itemData.Idx));
            }
            else if (itemData.ImageKey == IMAGE_KEY)
            {
                this.AnnotEditView.GetComponent<AnnotEditViewCtrl>().
                    EditImageAnnot(this.ImageAnnotView.GetComponent<ImageAnnotViewCtrl>().GetObject(itemData.Idx));
            }
            else if (itemData.ImageKey == LINK_KEY)
            {
                this.AnnotEditView.GetComponent<AnnotEditViewCtrl>().
                    EditLinkAnnot(this.AnnotView.GetComponent<LinkAnnotViewCtrl>().GetObject(itemData.Idx));
            }
            this.AnnotEditView.SetActive(true);
        }
        this.RotateToItem(_item);
    }

    //PRIVATE
    /**
     * @brief 리스트 뷰 아이템의 어노테이션 오브젝트가 조준 되도록 파노라믹 뷰를 회전 시킴
     * @param _item 조준 될 어노테이션의 리스트 뷰 아이템
     */
    void RotateToItem(ListViewItem _item)
    {
        if (_item != null)
        {
            ItemData itemData = _item.Tag as ItemData;

            PanoramicViewCtrl.Rotate(itemData.Object);
        }
    }

    /**
     * @brief 헤더에 컬럼 텍스트 추가하기
     */
    void AddColumns()
    {
        this.ListView.SuspendLayout();
        {
            AddColumnHeader("Name");
            AddColumnHeader("Angle");
            AddColumnHeader("Edit");
        }
        this.ListView.ResumeLayout();
    }

    /**
     * @brief 헤더에 컬럼 텍스트 추가하기
     * @param _title 컬럼 타이틀
     */
    void AddColumnHeader(string _title)
    {
        ColumnHeader columnHeader = new ColumnHeader();
        columnHeader.Text = _title;
        this.ListView.Columns.Add(columnHeader);
    }

    /**
     * @brief 아이템 데이타 생성
     * @param _imgKey 리스트 아이템의 이미지 키 값, {"Text", "Image"}
     * @param _idx 인텍스, 어노테이션 종류별로 유일함, 0~
     * @param _name 이름
     * @param _angle 파노라믹 뷰 위의 위치
     * @param _ctime 생성 시간
     * @param _mtime 수정된 시간
     * @param _obj 어노테이션 오브젝트의 게임 오브젝트
     * @returns ItemData 인스턴스 
     */
    ItemData NewItemData(string _imgKey, int _idx, string _name, Vector3 _angle, string _ctime, string _mtime, GameObject _obj)
    {
        ItemData item = new ItemData();
        item.ImageKey = _imgKey;
        item.Idx = _idx;
        item.Name = _name;
        item.Angle = _angle;
        item.CreatedTime = _ctime;
        item.ModifiedTime = _mtime;
        item.Object = _obj;

        return item;
    }

    /**
     * @brief 리스트 뷰 아이템 생성
     * @param _item 사용할 내용이 있는 아이템 데이타
     * @returns ListViewItem 인스턴스 
     */
    ListViewItem NewListViewItem(ItemData _item)
    {
        string[] subItemTexts = new string[]
        {
            _item.Name,
            AnnotListCtrl.AngleToString(_item.Angle),
            EDIT_KEY
        };

        ListViewItem listViewItem = new ListViewItem(subItemTexts);
        listViewItem.ImageKey = _item.ImageKey;
        listViewItem.Tag = _item;

        return listViewItem;
    }

    /**
     * @brief 텍스트 어노테이션 리스트 아이템 추가
     * @param _obj 텍스트 어노테이션 오브텍트의 GameObject 인스턴스
     */
    void AddTextAnnotObjCtrl(GameObject _obj)
    {
        TextAnnotObjCtrl ctrl = _obj.GetComponent<TextAnnotObjCtrl>();
        if (ctrl)
        {
            ItemData itemData = NewItemData(TEXT_KEY,
                                            ctrl.Idx,
                                            ctrl.Name,
                                            ctrl.Angle,
                                            ctrl.CreatedTime,
                                            ctrl.ModifiedTime,
                                            ctrl.gameObject);
            ListViewItem item = NewListViewItem(itemData);
            this.ListView.Items.Add(item);
        }
    }

    /**
     * @brief 이미지 어노테이션 리스트 아이템 추가
     * @param _obj 이미지 어노테이션 오브텍트의 GameObject 인스턴스
     */
    void AddImageAnnotObjCtrl(GameObject _obj)
    {
        ImageAnnotObjCtrl ctrl = _obj.GetComponent<ImageAnnotObjCtrl>();
        if (ctrl)
        {
            ItemData itemData = NewItemData(IMAGE_KEY,
                                            ctrl.Idx,
                                            ctrl.Name,
                                            ctrl.Angle,
                                            ctrl.CreatedTime,
                                            ctrl.ModifiedTime,
                                            ctrl.gameObject);
            ListViewItem item = NewListViewItem(itemData);
            this.ListView.Items.Add(item);
        }
    }

    /**
     * @brief 링크 어노테이션 리스트 아이템 추가
     * @param _obj 링크 어노테이션 오브텍트의 GameObject 인스턴스
     */
    void AddLinkAnnotObjCtrl(GameObject _obj)
    {
        LinkAnnotObjCtrl ctrl = _obj.GetComponent<LinkAnnotObjCtrl>();
        if (ctrl)
        {
            ItemData itemData = NewItemData(LINK_KEY,
                                            ctrl.Idx,
                                            ctrl.Name,
                                            ctrl.Angle,
                                            ctrl.CreatedTime,
                                            ctrl.ModifiedTime,
                                            ctrl.gameObject);
            ListViewItem item = NewListViewItem(itemData);
            this.ListView.Items.Add(item);
        }
    }

    /**
     * @brief 어노테이션 리스트의 리스트 아이템 설정
     * @param _filter 리스트 필터 값, {FILTER_ALL, FILTER_TEXT, FILTER_IMAGE}
     * @param _sort 정렬 종류 값, {SORT_NAME, SORT_TIME}
     * @param _order 정렬 차순 값, 0: 오름차순 1:내림차순
     */
    void BuildList(string _filter, string _sort, int _order)
    {
        this.CUR_SORT = _sort;
        this.CUR_ORDER = _order;

        this.ListView.Items.Clear();

        List<GameObject> objs = new List<GameObject>();
        if (_filter == FILTER_ALL || _filter == FILTE_TEXT)
        {
            foreach (GameObject obj in this.TextAnnotView.GetComponent<TextAnnotViewCtrl>().List())
            {
                objs.Add(obj);
            }
        }

        if (_filter == FILTER_ALL || _filter == FILTE_IMAGE)
        {
            foreach (GameObject obj in this.ImageAnnotView.GetComponent<ImageAnnotViewCtrl>().List())
            {
                objs.Add(obj);
            }
        }

        if (_filter == FILTER_ALL || _filter == FILTE_LINK)
        {
            foreach (GameObject obj in this.AnnotView.GetComponent<LinkAnnotViewCtrl>().List())
            {
                objs.Add(obj);
            }
        }

        if (1 < objs.Count)
        {
            objs.Sort(delegate (GameObject a, GameObject b)
            {
                TextAnnotObjCtrl ta = a.GetComponent<TextAnnotObjCtrl>();
                ImageAnnotObjCtrl ia = a.GetComponent<ImageAnnotObjCtrl>();
                LinkAnnotObjCtrl la = a.GetComponent<LinkAnnotObjCtrl>();

                TextAnnotObjCtrl tb = b.GetComponent<TextAnnotObjCtrl>();
                ImageAnnotObjCtrl ib = b.GetComponent<ImageAnnotObjCtrl>();
                LinkAnnotObjCtrl lb = b.GetComponent<LinkAnnotObjCtrl>();

                string va = "";
                string vb = "";
                if (_sort == SORT_NAME)
                {
                    va = (ta != null) ? ta.Name : (ia != null) ? ia.Name : (la != null) ? la.Name : "";
                    vb = (tb != null) ? tb.Name : (ib != null) ? ib.Name : (lb != null) ? lb.Name : "";
                }
                else if (_sort == SORT_TIME)
                {
                    va = (ta != null) ? ta.ModifiedTime : (ia != null) ? ia.ModifiedTime : (la != null) ? la.ModifiedTime : "";
                    vb = (tb != null) ? tb.ModifiedTime : (ib != null) ? ib.ModifiedTime : (lb != null) ? lb.ModifiedTime : "";
                }

                int r = string.Compare(va, vb);
                return (r == 0) ? 0 : (_order == 0) ? r : -r;
            });
        }

        foreach(GameObject obj in objs)
        {
            if (obj.GetComponent<TextAnnotObjCtrl>() != null)
            {
                AddTextAnnotObjCtrl(obj);
            }
            else if (obj.GetComponent<ImageAnnotObjCtrl>() != null)
            {
                AddImageAnnotObjCtrl(obj);
            }
            else if (obj.GetComponent<LinkAnnotObjCtrl>() != null)
            {
                AddLinkAnnotObjCtrl(obj);
            }
        }
    }

    //PUBLIC
    /**
     * @brief 어노테이션의 리스트 갱신
     */
    public void RebuildList()
    {
        this.BuildList(this.FILTERS[this.CUR_FILTER], this.CUR_SORT, this.CUR_ORDER);
    }

    //STATIC
    /**
     * @brief 어노테이션의 위치 값에 해당하는 문자열을 리스트에 표시될 포멧으로 만들어 리턴
     * @param _angle 파노라믹 뷰 위의 위치 값
     * @returns 리스트에 표시될 위치 문자열
     */
    static public string AngleToString(Vector3 _angle)
    {
        return _angle.x.ToString("N1") + " , " + _angle.y.ToString("N1");
    }
}
