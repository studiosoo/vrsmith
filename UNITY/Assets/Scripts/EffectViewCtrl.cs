﻿/**
 * @file EffectViewCtrl.cs 
 * @date 2018/08/10 
 * @author KIMJISOO(kind.jamie@gmail.com)
 * @brief 이펙트 뷰 컨트롤 스크립트
 */
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/**
 * @class EffectViewCtrl
 * @date 2018/08/10
 * @brief 이펙트 뷰 컨트롤
 */
public class EffectViewCtrl : MonoBehaviour {

    //SINGLETON
    static public EffectViewCtrl Instance = null;   /** 싱글턴 인스턴스 */

    public GameObject FadeView;     /** 페이드인/아웃 뷰 */
    public float FadeInDuration;    /** 페이드인 진행 시간 */
    public float FadeOutDuration;   /** 페이드아웃 진행 시간 */
    float OnFadeInStart = -1.0f;    /** 페이드인 시작 시간 */
    float OnFadeOutStart = -1.0f;   /** 페이드아웃 시작 시간 */

    public GameObject ProgressView; /** 프로그레스 뷰 */
    public GameObject Spinner;      /** 스핀 이미지 오브젝트 */
    float rotateSpeed = 240f;       /** 회전 속도 */

    /**
     * @brief 앱 구동시, 싱글턴 인스턴스 설정
     */
    void Awake()
    {
        if (Instance == null) Instance = this;
    }

    /**
     * @brief 뷰 비활성화
     */
    void Start()
    {
        this.FadeView.SetActive(false);
        this.ProgressView.SetActive(false);
    }

    /**
     * @brief 이펙트 표시
     */
    void Update()
    {
        if (0.0f < this.OnFadeInStart)
        {
            float a = 1.1f - (Time.time - this.OnFadeInStart) / this.FadeInDuration;
            this.FadeView.GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, a);
            if (a <= 0.0f)
            {
                this.OnFadeInStart = -1.0f;
                this.FadeView.SetActive(false);
            }
        }
        if (0.0f < this.OnFadeOutStart)
        {
            float a = (Time.time - this.OnFadeOutStart) / this.FadeOutDuration - 0.1f;
            this.FadeView.GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, a);
            if (1.0f <= a)
            {
                this.OnFadeOutStart = -1.0f;
                this.FadeView.SetActive(false);
            }
        }
        if (this.ProgressView.activeSelf)
        {
            this.Spinner.transform.Rotate(0f, 0f, -rotateSpeed * Time.deltaTime);
        }
    }

    /**
    * @brief 페이드 인 시작 
    */
    void _FadeIn()
    {
        this.OnFadeOutStart = -1.0f;
        this.OnFadeInStart = Time.time;
        this.FadeView.SetActive(true);
        this.FadeView.GetComponent<Image>().color = Color.black;
    }
   
    /**
    * @brief 페이드 아웃 시작 
    */
    void _FadeOut()
    {
        this.OnFadeInStart = -1.0f;
        this.OnFadeOutStart = Time.time;
        this.FadeView.SetActive(true);
        this.FadeView.GetComponent<Image>().color = Color.clear;
    }

    /**
    * @brief 프로그래스 활성화 설정
    * @parma _on 활성화 여부
    */
    void _Progress(bool _on)
    {
        this.ProgressView.SetActive(_on);
    }

    //STATIC
    /**
    * @brief 페이드 인 시작 
    */
    static public void FadeIn()
    {
        if (EffectViewCtrl.Instance != null) EffectViewCtrl.Instance._FadeIn();
    }

    /**
    * @brief 페이드 아웃 시작 
    */
    static public void FadeOut()
    {
        if (EffectViewCtrl.Instance != null) EffectViewCtrl.Instance._FadeOut();
    }

    /**
    * @brief 프로그래스 활성화 설정
    * @parma _on 활성화 여부
    */
    static public void Progress(bool _on)
    {
        if (EffectViewCtrl.Instance != null) EffectViewCtrl.Instance._Progress(_on);
    }
}
